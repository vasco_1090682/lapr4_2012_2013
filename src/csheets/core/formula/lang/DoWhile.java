/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import static csheets.core.formula.lang.Do.parameters;
import java.lang.reflect.Method;

/**
 * Função DoWhile
 * @author Cláudio 
 */
public class DoWhile implements Function {
    
    //private static boolean flag = false;
    private String identifier;
    private Method method;
    /**
     *
     */
    public static final FunctionParameter[] parameters = new FunctionParameter[] {
            new FunctionParameter(Value.Type.UNDEFINED, "Expressão1", false,
                    "A condition to evaluate before proceeding"),
		new FunctionParameter(Value.Type.UNDEFINED, "Expressão2", false,
			"An expression to evaluate"),
               new FunctionParameter(Value.Type.BOOLEAN, "Condição", false,
                         "An expression to evaluate")        
    };
    @Override
    public String getIdentifier() {
        return "DW";
    }
    
    /**
     *
     * @param identifier - identificador da função DoWhile ("DW" por default)
     */
    public void setIdentifier(String identifier){
        this.identifier = identifier;
    }
    @Override
    public Value applyTo(Expression[] arguments) throws IllegalValueTypeException {
        
        Value v = new Value();
       //System.out.println("ARGUMENTO 0:"+arguments[0].toString()+" - "+arguments[0].evaluate().toBoolean());
       //if(arguments[0].evaluate().toBoolean()!=false){
       //Expression ex = arguments[0];

           // if(arguments[0].evaluate().toBoolean()==!false){
        //System.out.println(arguments[2].toString());
        //System.out.println(arguments[2].evaluate());
        
                v=arguments[0].evaluate();  
//             for (int i = 0; i < 3; i++) {
//                 
//                System.out.println(arguments[i].evaluate());
//             
//             }
          //  }
                //System.out.println("Argumento 0: True");
                //while(ex.evaluate().toBoolean()!=false){
                       // System.out.println("LENGTH:"+arguments.length);
                       // for (int i = 1; i< arguments.length ; i++) {
                            //System.out.println(i); 
//                         v = arguments[0].evaluate(); 
//                         v = arguments[1].evaluate();
//                         v = arguments[2].evaluate();
                         
                        // }
                 // System.out.println("Depois do for:"+arguments[0].evaluate().toBoolean());   
        //}
                   
     //  }
                //flag = false; 
                    return v;  
       
       
 

    }


    public FunctionParameter[] getParameters() {
		return parameters;
    }


    public boolean isVarArg() {
		return method.isVarArgs();
    }
    
}
