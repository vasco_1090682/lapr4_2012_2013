/**
 * <p> <b>Diagrams</b>
 * <p> <b>Sequence Diagram</b>
 * <p> <img src="doc-files/writexml_sequence.png">
 * <p> <img src="doc-files/readxml_sequence.png"> 
 * <p> <b>Class Diagram</b>
 * <p> <img src="doc-files/diagramaclasses_hibernate.png"> 
 * <p> <b>Use Case</b> 
 * <p> <img src="doc-files/usecase_persistencia_it3.png">
 */

/*
 @startuml doc-files/usecase_persistencia_it3.png
 User -right-> (Transformar versao antiga em actual)
 User -left-> (Remover versoes do ficheiro)
 User -up-> (Undo e redo de operacoes)
 @enduml
 * 
 @startuml doc-files/diagramaclasses_hibernate.png
 class HibernateUtil {
 -{static}SessionFactory session
 +{static}SessionFactory getSession()
 }
 class XMLMap {
 -int id
 -java.sql.Timestamp time
 -byte[] blob
 +XMLMap()
 +XMLMap (Workbook workbook)
 +setBlob(byte[] b)
 +getBlob()
 +setId(int i)
 +getId()
 +setTime(java.sql.Timestamp time)
 +getTime()
 }
 @enduml
 @startuml doc-files/writexml_sequence.png
 actor User 
 User-> XMLCodec : Gravar Ficheiro
 XMLCodec -> XMLMap : new XMLMap()
 XMLCodec -> HibernateUtil : getSession().openSession()
 HibernateUtil -> XMLCodec : session()
 XMLCodec -> Transaction : beginTransaction()
 XMLCodec -> Session : save(XMLMap map)
 XMLCodec -> Session : close()
 XMLCodec -> IDataSet
 IDataSet -> IDatabaseConnection : createDataSet()
 XMLCodec -> FlatXmlDataSet : write(fullDataSet, stream)
 @enduml
 @startuml doc-files/readxml_sequence.png
 actor User 
 User-> XMLCodec : Abrir Ficheiro
 XMLCodec -> FlatXmlDataSet : new FlatXmlDataSet(new FlatXmlProducer(new InputSource(stream)))
 XMLCodec -> DriverManager : getConnection("jdbc:hsqldb:mem:cleansheets", "sa", "")
 XMLCodec -> IDatabaseConnection : new DatabaseConnection(jdbcConnection)
 XMLCodec -> Session
 Session -> HibernateUtil : getSession().openSession()
 HibernateUtil -> Session : session
 XMLCodec -> Transaction : session.beginTransaction()
 XMLCodec -> DatabaseOperation : CLEAN_INSERT.execute(connection, dataset)
 XMLCodec -> Query : session.createQuery("from csheets.ext.persistencia.XMLMap")
 Query -> XMLCodec :  List<Workbook>workbooks
 @enduml
 */
package csheets.ext.persistencia.hibernate;
