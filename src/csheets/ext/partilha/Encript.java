package csheets.ext.partilha;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Esta classe fornece os métodos que fazem a encriptação e desencriptação de uma string,
 * que nesta aplicação servirá para encriptar as passwords
 * @author Vasco Ribeiro
 */
public class Encript {
    /**
     * Método que recebendo uma string que corresponde a uma password, encripta essa string
     * com uma chave de encriptação definida pelo programador
     * @param password Password que se pretende encriptar
     * @return Uma string que corresponde à password encriptada
     */
    public static String encode(String password) {
        try {
            DESKeySpec keySpec = new DESKeySpec("Lapr4SecretKey".getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key = keyFactory.generateSecret(keySpec);
            sun.misc.BASE64Encoder base64encoder = new BASE64Encoder();
            Cipher cipher = Cipher.getInstance("DES");

            byte[] bytesPassword = password.getBytes("UTF8");

            cipher.init(Cipher.ENCRYPT_MODE, key);
            String encodedPassword = base64encoder.encode(cipher.doFinal(bytesPassword));
            return encodedPassword;
        } catch (InvalidKeyException ex) {
        } catch (NoSuchAlgorithmException ex) {
        } catch (NoSuchPaddingException ex) {
        } catch (UnsupportedEncodingException ex) {
        } catch (InvalidKeySpecException ex) {
        } catch (IllegalBlockSizeException ex) {
        } catch (BadPaddingException ex) {
        } catch (IOException ex) {
        }
        return null;
    }

    /**
     * Método que desencripta uma password encriptada, a chave de desincriptação é a mesma usada
     * para encriptar, definida pelo programador
     * @param encodedPassword Password para desencriptar
     * @return String que representa uma password, no seu estado original
     */
    public static String decode(String encodedPassword) {
        try {
            DESKeySpec keySpec = new DESKeySpec("Lapr4SecretKey".getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key = keyFactory.generateSecret(keySpec);
            Cipher cipher = Cipher.getInstance("DES");

            sun.misc.BASE64Decoder base64decoder = new BASE64Decoder();

            byte[] encodedBytesPassword = base64decoder.decodeBuffer(encodedPassword);


            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] bytesPassword = (cipher.doFinal(encodedBytesPassword));
            String password = new String(bytesPassword);
            return password;

        } catch (InvalidKeyException ex) {
        } catch (NoSuchAlgorithmException ex) {
        } catch (NoSuchPaddingException ex) {
        } catch (UnsupportedEncodingException ex) {
        } catch (InvalidKeySpecException ex) {
        } catch (IllegalBlockSizeException ex) {
        } catch (BadPaddingException ex) {
        } catch (IOException ex) {
        }
        return null;
    }
}
