/**
 * <p> <b>Diagrams</b>
 * <p> <b>Week 1<b>
 * <p> <b>Sequence Diagram</b>
 * <p> <img src="doc-files/criar_partilha_sequence.png">
 * <p> <img src="doc-files/aceder_partilha_sequence.png"> 
 * <p> <b>Class Diagram</b>
 * <p> <img src="doc-files/partilha_class_diagram.png"> 
 * <p> <b>Use Case</b> 
 * <p> <img src="doc-files/partilha_usecase.png">
 *
 * <p> <b>Week 2<b> 
 * <p> <b>Sequence Diagram</b>
 * <p> <img src="doc-files/IT2_Partilha_Trilho5_Sequencia_CriarPartilha.png">
 * <p> <img src="doc-files/IT2_Partilha_Trilho5_Sequencia_ProcurarPartilhas.png">
 * <p> <img src="doc-files/IT2_Partilha_Trilho5_Sequencia_AcederPartilha.png">
 * <p> <b>Class Diagram</b> 
 * <p> <img src="doc-files/IT2_Partilha_Trilho5_Classes.png"> 
 * <p> <b>Use Case</b>
 * <p> <img src="doc-files/IT2_Partilha_Trilho5_UseCase.png">
 * 
 * <p> <b>Week 3<b> 
 * <p> <b>Sequence Diagram</b>
 * <p> <img src="doc-files/IT3_Partilha_Trilho5_Sequencia_ActivarPartilha.png">
 * <p> <img src="doc-files/IT3_Partilha_Trilho5_Sequencia_ProcurarPartilhas.png">
 * <p> <img src="doc-files/IT3_Partilha_Trilho5_Sequencia_AcederPartilha.png"> 
 * <p> <b>Class Diagram</b> 
 * <p> <img src="doc-files/IT3_Partilha_Trilho5_Classes.png"> 
 * <p> <b>Use Case</b>
 * <p> <img src="doc-files/IT3_Partilha_Trilho5_UseCase.png">
 */

/*
 * 
 @startuml doc-files/criar_partilha_sequence.png
 actor User 
 User-> PartilhaPanel : Criar Partilha
 PartilhaPanel -> PartilhaServer : new PartilhaServer
 PartilhaPanel -> PartilhaServer : start()
 PartilhaServer -> PartilhaServer : run()
 PartilhaServer -> PartilhaServer : celulasPartilhadas()
 @enduml
    
 @startuml doc-files/aceder_partilha_sequence.png
 actor User 
 User-> PartilhaPanel : Aceder a Partilha
 PartilhaPanel -> PartilhaClient: new PartilhaClient
 PartilhaPanel -> PartilhaClient : start()
 PartilhaClient -> PartilhaClient : run()
 PartilhaClient -> PartilhaClient : setCelulas()
 @enduml
    
 @startuml doc-files/partilha_usecase.png
 User -right-> (Criar Partilha)
 User -up-> (Aceder a Partilha)
 @enduml
    
 @startuml doc-files/partilha_class_diagram.png
 class PartilhaServer {
 -{static}DataOutputStream outstream
 -UIController uiController
 -ArrayList<Cell> sharedcells
 -String area
 -int porta
 -int r1
 -int c1
 -int r2
 -int c2
 +PartilhaServer(String a, int p, UIController ui)
 +void run()
 -void celulasPartilhadas()
 }
 class PartilhaClient {
 -{static}DataOutputStream outstream
 -String ip
 -UIController uiController
 -String area
 -String arearecebida
 -ArrayList<String> content
 -int porta
 +PartilhaClient(String ipv4, String a, int p, UIController ui)
 +void run()
 -void setCelulas(int col1, int row1, int col2, int row2, ArrayList<String> conteudo)
 }
 class ExtensionPartilha {
 }
 class PartilhaPanel {
 }
 class UIExtensionPartilha {
 }
 ExtensionPartilha -> UIExtensionPartilha
 UIExtensionPartilha -> PartilhaPanel
 PartilhaPanel -> PartilhaServer
 PartilhaPanel -> PartilhaClient
 @enduml
 */
/*
 * @startuml doc-files/IT2_Partilha_Trilho5_Sequencia_CriarPartilha.png
 * participant "User"
 * participant "PartilhaUI"
 * participant "PartilhaServerController"
 * participant "UDPServerController"
 * participant "PartilhaServer"
 * participant "UDPServer"
 * 
 * User -> PartilhaUI : enter name
 * User -> PartilhaUI : enter area
 * User -> PartilhaUI : choose option "Create Server"
 * 
 * PartilhaUI -> PartilhaServerController : create(area, 500, uiController)
 * PartilhaServerController -> PartilhaServer : new PartilhaServer(area, p, ui)
 * PartilhaServer --> PartilhaServerController : <<created>>
 * PartilhaServerController -> PartilhaServer : start()
 * activate PartilhaServer
 * PartilhaServer -> PartilhaServer : setBondaries()
 * PartilhaServer -> PartilhaServer : celulasPartilhadas()
 * PartilhaServer -> PartilhaServer : addCellListener()
 * PartilhaServer -> PartilhaServer : addCellColor()
 * loop while online
 *      PartilhaServer -> PartilhaServer : updateCeculasPartilhadas(Cell cell)
 * end
 * PartilhaServerController -> PartilhaServer : stopServer()
 * deactivate PartilhaServer
 * 
 * PartilhaUI -> UDPServerController : create(name, area)
 * UDPServerController -> UDPServer : new UDPServer(name, area)
 * UDPServer --> UDPServerController : <<created>>
 * UDPServerController -> UDPServer : start()
 * activate UDPServer
 * UDPServerController -> UDPServer : stopServer()
 * deactivate UDPServer
 * @enduml
 * 
 * @startuml doc-files/IT2_Partilha_Trilho5_Sequencia_ProcurarPartilhas.png
 * participant "User"
 * participant "PartilhaUI"
 * participant "UDPClientController"
 * participant "UDPClient"
 * 
 * User -> PartilhaUI : choose option "Find servers"
 * 
 * PartilhaUI -> UDPClientController : create(model)
 * UDPClientController -> UDPClient : new UDPClient(model)
 * UDPClient --> UDPClientController : <<created>>
 * UDPClientController -> UDPClient : start()
 * activate UDPClient
 * UDPClientController -> UDPClient : leaveServer()
 * deactivate UDPClient
 * @enduml
 * 
 * @startuml doc-files/IT2_Partilha_Trilho5_Sequencia_AcederPartilha.png
 * participant "User"
 * participant "PartilhaUI"
 * participant "UDPClientController"
 * participant "PartilhaClientController"
 * participant "PartilhaClient"
 * 
 * User -> PartilhaUI : choose option "Join Server"
 * 
 * PartilhaUI -> UDPClientController : getServer(name, area)
 * UDPClientController --> PartilhaUI : <<return server ip>
 * PartilhaUI -> PartilhaClientController : create(ip, 500, uiController)
 * PartilhaClientController -> PartilhaClient : new PartilhaClient(ipv4, p, ui)
 * PartilhaClient --> PartilhaClientController : <<created>>
 * PartilhaClientController -> PartilhaClient : start()
 * activate PartilhaClient
 * PartilhaClient -> PartilhaClient : setBondaries()
 * PartilhaClient -> PartilhaClient : addCellListener()
 * PartilhaClient -> PartilhaClient : addCellColor()
 * loop while connected
 *      PartilhaClient -> PartilhaClient : updateCeculasPartilhadas(Cell cell)
 * end
 * PartilhaClientController -> PartilhaClient : stopServer()
 * deactivate PartilhaClient
 * @enduml
 * 
 * @startuml doc-files/IT2_Partilha_Trilho5_Classes.png
 * class Partilha{
 *  - name : String
 *  - area : String
 *  - password : String
 *  - permissions: PermissionsTypes (Enum)
 *  - serverIp : InetAddress
 *  - online : boolean
 *  - Partilha(String name, String area, InetAddress serverIp, boolean online)
 *  - String getName()
 *  - String getArea(
 *  - String getPassword()
 *  - PermissionsTypes getPermissions()
 *  - InetAddress getServerIp()
 *  - boolean isOnline()
 *  - setName(String name)
 *  - setArea(String area)
 *  - setPassword(String password)
 *  - setPermissions(PermissionsTypes permissions)
 *  - setServerIp(InetAddress serverIp)
 *  - setOnline(boolean online)
 * }
 * class PartilhaClient{
 *  - outstream : DataOutputStream
 *  - instream : DataInputStream
 *  - sock : Socket
 *  - ip : String
 *  - uiController : UIController
 *  - arearecebida : String
 *  - content : ArrayList<String>
 *  - porta : int
 *  - r1 : int
 *  - r2 : int
 *  - c1 : int
 *  - c2 : int
 *  - connected : boolean
 *  - PartilhaClient(String ipv4, int p, UIController ui)
 *  - run()
 *  - setCelulas(int col1, int row1, int col2, int row2, ArrayList<String> conteudo)
 *  - leaveServer()
 *  - addCellListener()
 *  - addCellColor()
 *  - setBondaries()
 *  - updateCeculasPartilhadas(Cell cell)
 *  - CellListener getCellListener()
 * }
 * class PartilhaServer{
 *  - outstream : DataOutputStream
 *  - instream : DataInputStream
 *  - serverSock : ServerSocket
 *  - clntSock : Socket
 *  - uiController : UIController
 *  - sharedcells : ArrayList<Cell>
 *  - area : String
 *  - porta : int
 *  - r1 : int
 *  - r2 : int
 *  - c1 : int
 *  - c2 : int
 *  - online : boolean
 *  - PartilhaServer(String a, int p, UIController ui)
 *  - run()
 *  - stopServer()
 *  - celulasPartilhadas()
 *  - addCellListener()
 *  - addCellColor()
 *  - setBondaries()
 *  - updateCeculasPartilhadas(Cell cell)
 *  - CellListener getCellListener()
 * }
 * class UDPClient{
 *  - servers : ArrayList<Partilha>
 *  - model : DefaultComboBoxModel<String>
 *  - connected : boolean
 *  - UDPClient(DefaultComboBoxModel<String> model)
 *  - run()
 *  - boolean containsServer(Partilha partilha)
 *  - Partilha getServer(String name, String area)
 *  - leaveServer()
 *  - removeServer(Partilha p)
 * }
 * class UDPServer{
 *  - serverInfo : String
 *  - online : boolean
 *  - UDPServer(String name, String area)
 *  - run()
 *  - stopServer()
 * }
 * 
 * Partilha - UDPClient
 * UDPClient - UDPServer
 * PartilhaClient - PartilhaServer
 * @enduml
 * 
 * @startuml doc-files/IT2_Partilha_Trilho5_UseCase.png
 * 
 * User -up-> (Criar partilha)
 * User -right-> (Procurar partilhas)
 * User -down-> (Aceder partilha)
 * 
 * @enduml
 */
/*
 * @startuml doc-files/IT3_Partilha_Trilho5_Sequencia_ActivarPartilha.png
 * participant "User"
 * participant "PartilhaUI"
 * participant "PartilhaServerController"
 * participant "UDPServerController"
 * participant "PartilhaServer"
 * participant "UDPServer"
 * 
 * User -> PartilhaUI : enter name
 * User -> PartilhaUI : enter password
 * User -> PartilhaUI : enter area
 * User -> PartilhaUI : enter permission
 * User -> PartilhaUI : choose option "Activate"
 * 
 * PartilhaUI -> PartilhaUI : new Partilha(name, password, area, permission)
 * PartilhaUI -> PartilhaServerController : create(Partilha, uiController)
 * PartilhaServerController -> PartilhaServer : new PartilhaServer(Partilha, uiController)
 * PartilhaServer --> PartilhaServerController : <<created>>
 * PartilhaServerController -> PartilhaServer : start()
 * activate PartilhaServer
 * PartilhaServer -> PartilhaServer : setBondaries()
 * PartilhaServer -> PartilhaServer : celulasPartilhadas()
 * PartilhaServer -> PartilhaServer : addCellListener()
 * PartilhaServer -> PartilhaServer : addCellColor()
 * loop while online
 *      PartilhaServer -> PartilhaServer : updateCeculasPartilhadas(Cell cell)
 * end
 * PartilhaServerController -> PartilhaServer : stopServer()
 * deactivate PartilhaServer
 * 
 * PartilhaUI -> UDPServerController : create(Partilha)
 * UDPServerController -> UDPServer : new UDPServer(Partilha)
 * UDPServer --> UDPServerController : <<created>>
 * UDPServerController -> UDPServer : start()
 * activate UDPServer
 * UDPServerController -> UDPServer : stopServer()
 * deactivate UDPServer
 * @enduml
 * 
 * @startuml doc-files/IT3_Partilha_Trilho5_Sequencia_ProcurarPartilhas.png
 * participant "User"
 * participant "PartilhaUI"
 * participant "UDPClientController"
 * participant "UDPClient"
 * 
 * User -> PartilhaUI : choose option "Find servers"
 * 
 * PartilhaUI -> UDPClientController : create()
 * UDPClientController -> UDPClient : new UDPClient()
 * UDPClient --> UDPClientController : <<created>>
 * UDPClientController -> UDPClient : start()
 * activate UDPClient
 * UDPClientController -> UDPClient : leaveServer()
 * deactivate UDPClient
 * @enduml
 * 
 * @startuml doc-files/IT3_Partilha_Trilho5_Sequencia_AcederPartilha.png
 * participant "User"
 * participant "PartilhaUI"
 * participant "UDPClientController"
 * participant "PartilhaClientController"
 * participant "PartilhaClient"
 * 
 * User -> PartilhaUI : choose option "Join Server"
 * User -> PartilhaUI : enter password
 * 
 * PartilhaUI -> UDPClientController : getServer(name, area)
 * UDPClientController --> PartilhaUI : <<return Partilha>>
 * PartilhaUI -> PartilhaClientController : create(Partilha, password, uiController)
 * PartilhaClientController -> PartilhaClient : new PartilhaClient(Partilha, password, ui)
 * PartilhaClient --> PartilhaClientController : <<created>>
 * PartilhaClientController -> PartilhaClient : start()
 * activate PartilhaClient
 * PartilhaClient -> PartilhaClient : setBondaries()
 * PartilhaClient -> PartilhaClient : addCellListener()
 * PartilhaClient -> PartilhaClient : addCellColor()
 * loop while connected
 *      PartilhaClient -> PartilhaClient : updateCeculasPartilhadas(Cell cell)
 * end
 * PartilhaClientController -> PartilhaClient : stopServer()
 * deactivate PartilhaClient
 * @enduml
 * 
 * @startuml doc-files/IT3_Partilha_Trilho5_Classes.png
 * class Partilha{
 *  - name : String
 *  - area : String
 *  - password : String
 *  - permissions: PermissionsTypes (Enum)
 *  - serverIp : InetAddress
 *  - online : boolean
 *  - Partilha(String name, String area, Partilha.PermissionsTypes permission, InetAddress serverIp, boolean online)
 *  - Partilha(String name, String area, String password, PermissionsTypes permission, boolean online)
 *  - String getName()
 *  - String getArea(
 *  - String getPassword()
 *  - PermissionsTypes getPermission()
 *  - InetAddress getServerIp()
 *  - boolean isOnline()
 *  - setName(String name)
 *  - setArea(String area)
 *  - setPassword(String password)
 *  - setPermission(PermissionsTypes permissions)
 *  - setServerIp(InetAddress serverIp)
 *  - setOnline(boolean online)
 * }
 * class PartilhaClient{
 *  - outstream : DataOutputStream
 *  - instream : DataInputStream
 *  - sock : Socket
 *  - uiController : UIController
 *  - arearecebida : String
 *  - content : ArrayList<String>
 *  - porta : int
 *  - r1 : int
 *  - r2 : int
 *  - c1 : int
 *  - c2 : int
 *  - connected : boolean
 *  - p : Partilha
 *  - password : String
 *  - modelServersConnectedToList : DefaultListModel<Object>
 *  - PartilhaClient(Partilha p, String password, DefaultListModel<Object> modelServersConnectedToList, UIController ui)
 *  - run()
 *  - setCelulas(int col1, int row1, int col2, int row2, ArrayList<String> conteudo)
 *  - leaveServer()
 *  - addCellListener()
 *  - addCellColor()
 *  - setBondaries()
 *  - updateCeculasPartilhadas(Cell cell)
 *  - CellListener getCellListener()
 * }
 * class PartilhaServer{
 *  - outstream : DataOutputStream
 *  - instream : DataInputStream
 *  - serverSock : ServerSocket
 *  - clntSock : Socket
 *  - uiController : UIController
 *  - sharedcells : ArrayList<Cell>
 *  - porta : int
 *  - r1 : int
 *  - r2 : int
 *  - c1 : int
 *  - c2 : int
 *  - online : boolean
 *  - p : Partilha
 *  - PartilhaServer(Partilha p, UIController ui)
 *  - run()
 *  - stopServer()
 *  - celulasPartilhadas()
 *  - addCellListener()
 *  - addCellColor()
 *  - setBondaries()
 *  - updateCeculasPartilhadas(Cell cell)
 *  - CellListener getCellListener()
 *  - Partilha getP()
 * }
 * class UDPClient{
 *  - servers : ArrayList<Partilha>
 *  - model : DefaultComboBoxModel<String>
 *  - connected : boolean
 *  - UDPClient(DefaultComboBoxModel<String> model)
 *  - run()
 *  - boolean containsServer(Partilha partilha)
 *  - Partilha getServer(String name, String area)
 *  - leaveServer()
 *  - removeServer(Partilha p)
 * }
 * class UDPServer{
 *  - serverInfo : String
 *  - online : boolean
 *  - p : Partilha
 *  - UDPServer(String name, String area)
 *  - run()
 *  - stopServer()
 *  - Partilha getP()
 * }
 * 
 * Partilha - UDPClient
 * Partilha - UDPServer
 * Partilha - PartilhaClient
 * Partilha - PartilhaServer
 * UDPClient - UDPServer
 * PartilhaClient - PartilhaServer
 * @enduml
 * 
 * @startuml doc-files/IT3_Partilha_Trilho5_UseCase.png
 * 
 * User -up-> (Activar partilha)
 * User -right-> (Procurar partilhas)
 * User -down-> (Aceder partilha)
 * 
 * @enduml
 */
package csheets.ext.partilha;
