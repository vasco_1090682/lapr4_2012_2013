/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macro.ui;

import csheets.core.IllegalValueTypeException;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.macro.ControllerMacro;
import csheets.ext.macro.Macro;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

/**
 *
 * @author Fábio Queirós
 */
public class EditMacroUI extends JFrame {

    private JPanel status;
    private JProgressBar progressBar;
    private String name;
    private JLabel statusMessage;
    private JButton save, run;
    private JTextArea output;
    private JScrollPane outputScroll, contentScroll;
    private JTextPane content;
    private ControllerMacro ctrlMacro;
    private Macro macros;
    private ArrayList<String> lines;
    private int cmbMacrosNamesIndex;
    public static final String TITLE = " > Edit Macro";
   
    /*
     * Build Window and Window Components
     */
    public EditMacroUI(Macro macro, ControllerMacro ctrlMacro, int index) {
        super(macro.getName() + TITLE);
        this.cmbMacrosNamesIndex=index;
        this.ctrlMacro = ctrlMacro;
        this.macros = macro;
        this.setMinimumSize(new Dimension(700, 400));
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new MacrosMenuUIListener());
        this.setSize(700, 500);
        this.setLocationRelativeTo(null);

        JMenuBar menu = new JMenuBar();
        menu.setLayout(new GridLayout(1, 2));
        this.setJMenuBar(menu);
        int buttonsWidth = (int) this.getWidth() / 5;
        mainButtonsListener bal = new mainButtonsListener();

        //Save Macro Button
        save = new JButton("save");
        save.setPreferredSize(new Dimension(buttonsWidth, 60));
        save.addActionListener(bal);
        menu.add(save);

        //Run Macro Button
        run = new JButton("run");
        run.setPreferredSize(new Dimension(buttonsWidth, 60));
        run.addActionListener(bal);
        menu.add(run);

        //Content Pane
        content = new JTextPane();
        String macroText = "";
        for (int i = 0; i < macro.getExpressions().size(); i++) {
            macroText = macroText + macro.getExpressions().get(i) + "\n";
        }
        content.setText(macroText);
        content.setFont(new Font("Calibri", Font.PLAIN, 20));
        contentScroll = new JScrollPane(content);
        this.getContentPane().add(contentScroll, BorderLayout.CENTER);

        //Left Side
        JPanel aux = new JPanel();
        aux.setPreferredSize(new Dimension(20, this.getHeight()));
        this.getContentPane().add(aux, BorderLayout.WEST);

        //Right Side
        JPanel aux2 = new JPanel();
        aux2.setPreferredSize(new Dimension(20, this.getHeight()));
        this.getContentPane().add(aux2, BorderLayout.EAST);

        //Output Area
        output = new JTextArea("Output.", 5, 50);
        output.setLineWrap(true);
        output.setEditable(false);
        outputScroll = new JScrollPane(output);
        outputScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        outputScroll.setBounds(20, 10, contentScroll.getWidth(), 100);
        this.getContentPane().add(outputScroll, BorderLayout.SOUTH);



    }


    private class MacrosMenuUIListener implements WindowListener {

        public MacrosMenuUIListener() {
        }

        @Override
        public void windowOpened(WindowEvent we) {
        }

        @Override
        public void windowClosing(WindowEvent we) {
            askToExitSave();
        }

        @Override
        public void windowClosed(WindowEvent we) {
        }

        @Override
        public void windowIconified(WindowEvent we) {
        }

        @Override
        public void windowDeiconified(WindowEvent we) {
        }

        @Override
        public void windowActivated(WindowEvent we) {
        }

        @Override
        public void windowDeactivated(WindowEvent we) {
        }
    }

    private class mainButtonsListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == save) {
                editMacro();
            } else if (e.getSource() == run) {
                try {
                    runMacro();
                } catch (FormulaCompilationException ex) {
                    Logger.getLogger(EditMacroUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalValueTypeException ex) {
                    Logger.getLogger(EditMacroUI.class.getName()).log(Level.SEVERE, null, ex);
                }  catch (ParseException ex) {
                    Logger.getLogger(EditMacroUI.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        }
    }
    /*
     * metoth for save edited macro
     * 
     */

    private void editMacro() {
        String[] liness = content.getText().split("\\n");
        lines = new ArrayList();
        for (int i = 0; i < liness.length; i++) {
            lines.add((String) liness[i]);
        }
        Macro macroEdit = new Macro(macros.getName(),"", lines);
        ctrlMacro.editMacro(macroEdit);
        JOptionPane.showMessageDialog(null, "Macro sucesseful edited");
        MacroUI.setComboBoxIndex(cmbMacrosNamesIndex);
        dispose();
    }
    /*
     * 
     *
     */

    private void runMacro() throws FormulaCompilationException, FormulaCompilationException, FormulaCompilationException, IllegalValueTypeException, ParseException, ParseException, ParseException, ParseException, ParseException {
        try {
            String vazia="";
            String result[];
            String[] liness = content.getText().split("\\n");
            int flagVar=0;
            int flagExp=0;
            lines = new ArrayList();
            for (int i = 0; i < liness.length; i++) {
                lines.add((String) liness[i]);
            }
            Macro macroEdit = new Macro(macros.getName(),"", lines);
            output.removeAll();
            result = ctrlMacro.runMacro(vazia,macroEdit).split("\n");
            int z=result.length-1;
            for (int i =z ; i >0 ; i--) {
                if((result[i].startsWith("Exp")) && (flagExp==0)){
                    vazia=vazia+result[i]+"\n";
                    flagExp=1;

                }
                if((result[i].startsWith("Temp")) && (flagVar==0)){
                    vazia=vazia+result[i]+"\n";
                    flagVar=1;
                }
                
            }
            output.setText(vazia);
        } catch (FormulaCompilationException ex) {
            output.setText("Error");
        } catch (IllegalValueTypeException ex) {
            output.setText("Error");
        }
    }

    /*
     * this method ask if you want to save or not the macro before close the window
     *  
     */
    private void askToExitSave() {
        int message = JOptionPane.showConfirmDialog(this, "Do You Want to Save Changes?", "Close Edit Macro", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        if (message == 0) {
            editMacro();
        } else if (message == 1) {
            MacroUI.setComboBoxIndex(cmbMacrosNamesIndex);
            dispose();
        }
    }
}
