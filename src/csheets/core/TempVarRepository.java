/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core;

import csheets.core.formula.lang.TempVarReference;
import java.util.ArrayList;


/**
 *
 * @author Fábio Queirós
 */
public class TempVarRepository {
    
    private static TempVarRepository instance = new TempVarRepository();
    private ArrayList<TempVarReference> arrayTempVar = new ArrayList<TempVarReference>();
    
    private TempVarRepository(){
    }
    
    public static TempVarRepository getInstance(){
        return instance;
    }
    
    public void addTempVarReference(TempVarReference tempVarRef){
        if(arrayTempVar.isEmpty()){
            arrayTempVar.add(tempVarRef);
        }else{
            if(!arrayTempVar.contains(tempVarRef)){
                arrayTempVar.add(tempVarRef);
            }
        }
    }
    
    public TempVarReference getTempVarReference(String nameTempVar){
        int arraySize = arrayTempVar.size();
        TempVarReference tempVarRef;
        for(int i = 0; i < arraySize; i++){
            tempVarRef = arrayTempVar.get(i);
            if(tempVarRef.getname().equalsIgnoreCase(nameTempVar)){
                return tempVarRef;
            }
        } 
        return null;
    }
    
    public void emptyTempVarRepository(){
        arrayTempVar.clear();
    }
    
}
