/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.persistencia;

import csheets.ext.Extension;
import csheets.ext.persistencia.ui.UIExtensionPersistencia;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Tiago
 */
public class ExtensionPersistencia extends Extension
{
    /** The name of the extension */
	public static final String NAME = "Versoes XML";

	/**
	 * Creates a Persistencia extension.
	 */
	public ExtensionPersistencia() 
        {
		super(NAME);
	}
	
	/**
	 * Returns the user interface extension of this extension (an instance of the class {@link  csheets.ext.persistencia.uiUIExtensionPersistencia}). <br/>
	 * In this extension example we are only extending the user interface.
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionPersistencia(this, uiController);
	}
}
