package csheets.ext.partilha.controllers;

import csheets.ext.partilha.Partilha;
import csheets.ext.partilha.PartilhaClient;
import csheets.ui.ctrl.UIController;
import javax.swing.DefaultListModel;

/**
 * Controlador do cliente TCP
 * @author Vasco Ribeiro
 */
public class PartilhaClientController {
    
    private PartilhaClient tcpclient;
    
    /**
     * Método que cria o cliente TCP
     * @param p Partilha associada ao cliente
     * @param password Password de acesso ao servidor
     * @param modelServersConnectedToList Dados da JList de servidores a que a instância está conectada
     * @param ui UIController
     */
    public void create(Partilha p, String password, DefaultListModel<Object> modelServersConnectedToList, UIController ui){
        tcpclient = new PartilhaClient(p, password, modelServersConnectedToList, ui);
    }
    
    /**
     * Método que dá início à thread
     */
    public void start(){
        tcpclient.start();
    }
    
    /**
     * Método que termina a thread
     */
    public void leaveServer(){
        tcpclient.leaveServer();
    }
}
