/*
 * Copyright (c) 2005 Einar Pehrson <einar@pehrson.nu>.
 *
 * This file is part of
 * CleanSheets - a spreadsheet application for the Java platform.
 *
 * CleanSheets is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CleanSheets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CleanSheets; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
header {package csheets.core.formula.newCompiler;}

{/**
 * A parser that generates expressions from lists of lexical tokens.
 * @author Fábio Queirós
 */}
class NewFormulaParser extends Parser;
options {
        k=2;
	buildAST = true;
	defaultErrorHandler = false;
}

/**
 * The start rule for formula expressions.
 */
/* content
	: ( expression | literal ) EOF
	; */

expression
	: CARDINAL! ( sequence | normal | whiledo | dowhile) EOF!
	;

normal
        :  comparison | atribuicao 
        ;

atribuicao
        :   ( CELL_REF | TEMPVAR ) ATTRIBUTION^ comparison
        ;

sequence
        : LCHA! atribuicao ( SEMI! ( atribuicao | comparison ) )* RCHA!
        ;

comparison
	: concatenation
		( ( EQ^ | NEQ^ | GT^ | LT^ | LTEQ^ | GTEQ^ ) concatenation )?
	;

whiledo
     /*   : WHILEDO^ LCHA! comparison (SEMI! (comparison | atribuicao))+ RCHA!*/
        : WD^ LCHA! comparison (SEMI! normal)+ RCHA!
        ;
/*
dowhile
        : DW^ LCHA! (atribuicao SEMI!) comparison RCHA!
        ;*/
dowhile
        : DW^ LCHA! dowhile_exp  RCHA!
        ;

dowhile_exp
        : (atribuicao (SEMI!)) (atribuicao (SEMI!)) comparison
        ;

concatenation
	: arithmetic_lowest
		( AMP^ arithmetic_lowest )*
	;

arithmetic_lowest
	:	arithmetic_low
		( ( PLUS^ | MINUS^ ) arithmetic_low )*
	;

arithmetic_low
	:	arithmetic_medium
		( ( MULTI^ | DIV^ ) arithmetic_medium )*
	;

arithmetic_medium
	:	arithmetic_high
		( POWER^ arithmetic_high )?
	;

arithmetic_high
	:	arithmetic_highest ( PERCENT^ )?
	;

arithmetic_highest
	:	( MINUS^ )? atom
	;

atom
	:	function_call
	|	reference
	|	literal
        |       TEMPVAR
        |       MACRO_CALL
	|	LPAR! comparison RPAR!
	;

function_call
	:	FUNCTION^ 
		( comparison ( SEMI! comparison )* )?
		RPAR!
	;

reference
	:	CELL_REF
		( ( COLON^ ) CELL_REF )?
	|	NAME
	;

literal
	:	NUMBER
	|	STRING
	;

{import csheets.core.formula.lang.Language;

/**
 * A lexer that splits a string into a list of lexical tokens.
 * @author Einar Pehrson
 */
@SuppressWarnings("all")}
class NewFormulaLexer extends Lexer;

options {
	k = 8;
	caseSensitive = false;
	caseSensitiveLiterals = false;
}

/* Function calls, named ranges and cell references */
protected LETTER: ('a'..'z') ;

ALPHABETICAL
	:	( ( LETTER )+ LPAR ) => ( LETTER )+ LPAR! {
			try {
				Language.getInstance().getFunction(#getText());
				$setType(FUNCTION);
			} catch (Exception ex) {
				throw new RecognitionException(ex.toString());
			}
		}
	|	/* ( LETTER ( LETTER | NUMBER )* EXCL )? */
		( ABS )? LETTER ( LETTER )?
		( ABS )? ( DIGIT )+ {
			$setType(CELL_REF);
		}
        |	( TVAR LETTER ( LETTER | DIGIT)* ) {
                    $setType(TEMPVAR);
                }
       /*  |     ( "whiledo"  LCHA! ) =>  ("whiledo"  RCHA!){
                    $setType(WD);
            |     ( "dowhile"  LCHA! ) =>  ("dowhile"  RCHA!){
                      $setType(DW);
              }   

*/
        |	( "exec"! LPAR! ( LETTER | DIGIT | ABS | MINUS )+ RPAR! ) {
                    $setType(MACRO_CALL);
                }
	;

/* String literals, i.e. anything inside the delimiters */
STRING
	:	QUOT!
		(options {greedy=false;}:.)*
		QUOT!
	;
protected QUOT: '"';

/* Numeric literals */
NUMBER: ( DIGIT )+ ( COMMA ( DIGIT )+ )? ;
protected DIGIT : '0'..'9' ;

/* Comparison operators */
CARDINAL        : "#" ;
WD              : "whiledo" ;
DW              : "dowhile" ;
EQ		: "=" ;
NEQ		: "<>" ;
LTEQ	: "<=" ;
GTEQ	: ">=" ;
GT		: '>' ;
LT		: '<' ;

/* Text operators */
AMP		: '&' ;

/*atribuicao*/
ATTRIBUTION :   ":=";

/* Arithmetic operators */
PLUS	: '+' ;
MINUS	: '-' ;
MULTI	: '*' ;
DIV		: '/' ;
POWER	: '^' ;
PERCENT : '%' ;

/* Reference operators */
protected ABS : '_' ;
protected EXCL:  '!'  ;
COLON	: ':' ;
TVAR    : '$' ;

/* Miscellaneous operators */
COMMA	: ',' ;
SEMI	: ';' ;
LPAR	: '(' ;
RPAR	: ')' ;
LCHA    : '{' ;
RCHA    : '}' ;

/* White-space (ignored) */
WS: ( ' '
	| '\r' '\n'
	| '\n'
	| '\t'
	)
	{$setType(Token.SKIP);}
	;