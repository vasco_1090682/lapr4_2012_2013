/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.CardinalExpressionCompiler;
import csheets.core.formula.newCompiler.NewFormulaLexer;
import csheets.core.formula.newCompiler.NewFormulaParser;
import java.io.StringReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fábio
 */
public class AttributionTest {
    
    public AttributionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of applyTo method, of class Attribution.
     */
    @Test
    public void testApplyTo() throws Exception {
        System.out.println("applyTo");
        Workbook workbook = new Workbook(1);
        Spreadsheet sheet = workbook.getSpreadsheet(0);
        Cell cell = sheet.getCell(new Address(0, 0));
        CardinalExpressionCompiler compiler = new CardinalExpressionCompiler();
        String line = "#A1:=5";
        NewFormulaParser parser = new NewFormulaParser(new NewFormulaLexer(new StringReader(line)));
        Expression expression = compiler.compile(cell, line);
        Value va = expression.evaluate();
        String expResult = va.toString();
        CellReference cr = new CellReference(cell);
        Attribution at = new Attribution();
        Value value = at.applyTo(cr, expression);
        String result = value.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIdentifier method, of class Attribution.
     */
    @Test
    public void testGetIdentifier() {
        System.out.println("getIdentifier");
        Attribution instance = new Attribution();
        String expResult = ":=";
        String result = instance.getIdentifier();
        assertEquals(expResult, result);
    }

}