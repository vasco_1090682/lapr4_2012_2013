package csheets.ext.macro;

import csheets.ext.macro.ui.MacrosUIExtension;
import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Fábio Queirós            
 */
public class MacrosExtension extends Extension {

    /**
     * String name
     */
    public static final String NAME = "Macros";

    /**
     *
     */
    public MacrosExtension() {
        super(NAME);
    }

    public UIExtension getUIExtension(UIController uiController) {
        return new MacrosUIExtension(this, uiController);
    }
}