/**
 * <p> <b>Diagrams</b>
 * <p>
 * <b>Sequence Diagram - SaveAs()</b>
 * <p>
 * <img src="doc-files/IT1_Persistencia_Trilho2_Sequencia1.png">
 * <p>
 * <b>Sequence Diagram - Load()</b>
 * <p>
 * <img src="doc-files/IT1_Persistencia_Trilho2_Sequencia2.png">
 * <p>
 * <b>Class Diagram</b>
 * <b>
 * <img src="doc-files/IT1_Persistencia_Trilho2_Classes.png">
 * <p>
 * <b>Use Case</b>
 * <img src="doc-files/IT1_Persistencia_Trilho2_UseCase.png">
 * <p>
 *
 * @author Cláudio Santos
 *
 */
/*
 * @startuml doc-files/IT1_Persistencia_Trilho2_Sequencia1.png
 * participant "User"
 * participant "Cleansheets"
 * participant "XMLCodec"
 * participant "DocumentBuilderFactory"
 * participant "DocumentBuilder"
 * participant "Document"
 * participant "Element"
 * participant "Cell"
 * participant "Transformer"
 *
 * User -> Cleansheets : SaveAs()
 *
 *
 * 
 * Cleansheets -> XMLCodec : write()
 * XMLCodec -> DocumentBuilderFactory : newInstance()
 * DocumentBuilderFactory -> DocumentBuilder : newDocumentBuilder()
 * DocumentBuilder -> Document : newDocumentBuilder()
 *
 *
 * Document -> Element: create elements
 * activate Element
 *
 * Element <- Document : 
 * Element-->Document: <<created>>
 * deactivate Element
 *
 *
 * Element->Cell: get all cell attributes
 * activate Cell
 * Cell-->Element: 
 * deactivate Cell
 * XMLCodec -> Transformer: newTransformer()
 * activate Transformer
 * Transformer --> XMLCodec: <<created>>
 * XMLCodec -> Transformer: transform()
 * deactivate Transformer
 *
 * @enduml
 * @startuml doc-files/IT1_Persistencia_Trilho2_Sequencia2.png
 * participant "User"
 * participant "Cleansheets"
 * participant "XMLCodec"
 * participant "DocumentBuilderFactory"
 * participant "DocumentBuilder"
 * participant "Document"
 * participant "Element"
 * participant "NodeList"
 * participant "Workbook"
 * 
 * 
 * User -> Cleansheets : Load()
 * 
 * 
 *  
 * Cleansheets -> XMLCodec : read()
 * XMLCodec -> Workbook : new Workbook()
 * activate Workbook
 * XMLCodec -> DocumentBuilderFactory : newInstance()
 * DocumentBuilderFactory -> DocumentBuilder : newDocumentBuilder()
 * DocumentBuilder -> Document : newDocumentBuilder()
 * 
 * 
 * Document -> Element: create elements
 * activate Element
 * 
 * Element-->Document: <<created>>
 * deactivate Element
 * 
 * 
 * Element->NodeList: get all attributes from given node
 * activate NodeList
 * NodeList-->Element: 
 * deactivate NodeList
 * 
 * Element -> Workbook : set attributes
 * Workbook --> Cleansheets : return Workbook
 * deactivate Workbook
 * 
 * @enduml
 * @startuml doc-files/IT1_Persistencia_Trilho2_Classes.png
 * class XMLCodec{
 *  - write(Workbook workbook, OutputStream stream)
 *  - Schema CarregarSchema(String name)
 *  - validarXML(Schema schema, Document document)
 *  - Workbook read(InputStream stream) 
 * }
 * 
 * class Codec{
 *  - write(Workbook workbook, OutputStream stream)
 *  - Workbook read(InputStream stream)
 * }  
 * 
 * class Workbook{
 *  - Spreadsheet getSpreadsheet(int index)
 *  - int getSpreadsheetCount()
 * }
 * class Cleansheets{
 *  - save(Workbook workbook)
 *	- saveAs(Workbook workbook, File file)()
 *	- load(File file)()
 * }
 * 
 * class CodecFactory{
 *  - String filename
 *  - String extension
 *	- Codec getCodec(File file)
 * }  
 * XMLCodec -> Codec
 * CodecFactory --> Codec
 * Cleansheets --> CodecFactory
 * Workbook - Cleansheets
 * 
 * @enduml
 * 
 * @startuml doc-files/IT1_Persistencia_Trilho2_UseCase.png
 * 
 * User -> (Save as .xml file)
 * User -> (Open .xml file)
 * 
 * @enduml
 */
package csheets.io;