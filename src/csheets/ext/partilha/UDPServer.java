package csheets.ext.partilha;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import javax.swing.JOptionPane;

/**
 * Esta classe é responsável por criar um servidor de protocolo UDP que anuncia na rede
 * a sua presença
 * @author Vasco Ribeiro
 */
public class UDPServer extends Thread {

    private String serverInfo;
    private boolean online;
    private Partilha p;

    /**
     * Contrutor do servidor UDP
     * @param p Partilha a anunciar na rede
     */
    public UDPServer(Partilha p) {
        this.p = p;
        serverInfo = p.getName() + ";" + p.getArea() + ";" + p.getPermission();      
    }
    
    @Override
    public void run() {
        try {
            online = true;

            DatagramSocket socket = new DatagramSocket();
            socket.setBroadcast(true);
            byte[] b = new byte[100];
            DatagramPacket p = new DatagramPacket(b, b.length);
            p.setAddress(InetAddress.getByAddress(new byte[]{(byte) 255, (byte) 255, (byte) 255, (byte) 255}));
            p.setPort(500);
            while (online) {
                String str = serverInfo + ";online";
                b = str.getBytes();
                p.setData(b);
                socket.send(p);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    JOptionPane.showMessageDialog(null, ex.toString());
                }
            }

            //mandar um último datagrama a indicar que vai fechar
            serverInfo += ";offline";
            b = serverInfo.getBytes();
            p.setData(b);
            socket.send(p);
        } catch (SocketException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }
    }

    /**
     * Método que muda a variável online para false, fazendo com que o servidor se desligue
     */
    public void stopServer() {
        online = false;
    }
    
    /**
     * Método de consulta
     * @return Partilha associada a este servidor
     */
    public Partilha getP() {
        return p;
    }

}
