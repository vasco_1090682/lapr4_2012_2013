/*
 * Copyright (c) 2005 Einar Pehrson <einar@pehrson.nu>.
 *
 * This file is part of
 * CleanSheets - a spreadsheet application for the Java platform.
 *
 * CleanSheets is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CleanSheets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CleanSheets; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package csheets.core.formula.compiler;

import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import antlr.ANTLRException;
import antlr.collections.AST;
import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.TempVarRepository;
import csheets.core.UpdateCell;
import csheets.core.Value;
import csheets.core.formula.BinaryOperation;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionCall;
import csheets.core.formula.Literal;
import csheets.core.formula.Reference;
import csheets.core.formula.UnaryOperation;
import csheets.core.formula.lang.Attribution;
import csheets.core.formula.lang.CellReference;
import csheets.core.formula.lang.Language;
import csheets.core.formula.lang.RangeReference;
import csheets.core.formula.lang.ReferenceOperation;
import csheets.core.formula.lang.TempVarReference;
import csheets.core.formula.lang.UnknownElementException;
import csheets.core.formula.lang.WhileDo;

import csheets.core.formula.newCompiler.NewFormulaLexer;
import csheets.core.formula.newCompiler.NewFormulaParser;
import csheets.core.formula.newCompiler.NewFormulaParserTokenTypes;
import javax.swing.JOptionPane;

/**
 * A compiler that generates Excel-style formulas from strings.
 *
 * @author Fábio Queirós
 */
public class CardinalExpressionCompiler implements ExpressionCompiler {

    /**
     * The character that signals that a cell's content is a formula ('#')
     */
    public static final char FORMULA_STARTER = '#';
    /**
     *
     */
    public String source;

    /**
     * Creates the Excel expression compiler.
     */
    public CardinalExpressionCompiler() {
    }

    /**
     * Retorna formula_starter (#)
     */
    public char getStarter() {
        return FORMULA_STARTER;
    }

    public Expression compile(Cell cell, String source) throws FormulaCompilationException {
        this.source = source;
        Expression exp = null;
        // Creates the lexer and parser
        NewFormulaParser parser = new NewFormulaParser(
                new NewFormulaLexer(new StringReader(source)));

        try {
            // Attempts to match an expression
            parser.expression();
        } catch (ANTLRException e) {
            throw new FormulaCompilationException(e);
        }

        AST node = parser.getAST();
        //exp = convert(cell,node);
        if (node.getType() == NewFormulaParserTokenTypes.SEMI) {
            SequenceThreadExecuter threadExecuter = new SequenceThreadExecuter(cell, node);
            Thread thread = new Thread(threadExecuter);
            thread.start();
            try {
                thread.join();  //waits for thread to end
                emptyTempVarStorage();  //erases all the temporary variables
                return threadExecuter.getExpression();
            } catch (Exception ex) {
                throw new FormulaCompilationException(ex);
            }
        } else {
            //Permite executar mais do que uma expressão numa só célula
            while (node.getNextSibling() != null) {
                //System.out.println("TESTE"+node.getFirstChild());
                // if(node.getFirstChild().getNextSibling()!=null){
                convert(cell, node);
                node = node.getNextSibling();
                //System.out.println("TESTE"+node.getFirstChild());
                // }
            }
            return convert(cell, node);
        }
    }

    /**
     * Converts the given ANTLR AST to an expression.
     *
     *
     *
     * @param cell Célula onde foi inserida a expressão
     * @param node the abstract syntax tree node to convert
     * @return the result of the conversion
     * @throws FormulaCompilationException
     */
    protected Expression convert(Cell cell, AST node) throws FormulaCompilationException {
        //System.out.println("Converting node '" + node.getText() + "' of tree '" + node.toStringTree() + "' with " + node.getNumberOfChildren() + " children.");
        if (node.getNumberOfChildren() == 0) {
            try {
                switch (node.getType()) {
                    case NewFormulaParserTokenTypes.NUMBER:
                        return new Literal(Value.parseNumericValue(node.getText()));
                    case NewFormulaParserTokenTypes.STRING:
                        return new Literal(Value.parseValue(node.getText(), Value.Type.BOOLEAN, Value.Type.DATE));
                    case NewFormulaParserTokenTypes.CELL_REF:
                        return new CellReference(cell.getSpreadsheet(), node.getText());
                    case NewFormulaParserTokenTypes.TEMPVAR:
                        return getTempVarExpression(node);
                    case NewFormulaParserTokenTypes.NAME:
                    /* return cell.getSpreadsheet().getWorkbook().
                     getRange(node.getText()) (Reference)*/
                }
            } catch (ParseException e) {
                throw new FormulaCompilationException(e);
            }
        }

        // Convert function call
        Function function = null;
        boolean whdo = false;
        boolean dowh = false;
        try {
            if (node.getType() == NewFormulaParserTokenTypes.WD) {
                function = Language.getInstance().getFunction("WD");
                //System.out.println("Função do Tipo WD");
            } else if (node.getType() == NewFormulaParserTokenTypes.DW) {
                function = Language.getInstance().getFunction("DW");
            } else {
                function = Language.getInstance().getFunction(node.getText());
            }
        } catch (UnknownElementException e) {
        }

        if (function != null) {
            List<Expression> args = new ArrayList<Expression>();
            List<Expression> args_temp = new ArrayList<Expression>();
            AST child = node.getFirstChild();
            AST child_temp = child;

            if (child != null) {
                //Expression arg = convert(cell,child);
                if (function.getIdentifier() != "DW") {
                    args.add(convert(cell, child));
                }
                if (function.getIdentifier() == "WD") {
                    try {

//                        System.out.println(args.get(0).toString());
                        if (args.get(0).evaluate().toBoolean() != false) {
                            whdo = true;
                            while (args.get(0).evaluate().toBoolean() != false) {
                                //      child=child_temp;

                                while (((child = child.getNextSibling()) != null) || (child.getFirstChild() != null)) {
                                    //  for (int i = 0; i < 2; i++) {
                                    //  if(args.get(0).evaluate().toBoolean() != false){

                                    args.add(convert(cell, child));
                                    //a++;

                                    //   }
                                    //convert(cell,child);


                                    // convert(cell,child);
                                    //   convert(cell,child);
                                    //   System.out.println("Teste"+convert(cell,child));
                                    //   }
                                    if ((child.getNextSibling() == null) && (args.get(0).evaluate().toBoolean() != false)) {
                                        child = child_temp;
//                                    System.out.println(child.getNextSibling().getFirstChild().getNextSibling());
                                    }
                                }

                                //convert(cell,child);
                            }
                        }
                    } catch (Exception e) {
                    }
//                    System.out.println(args.get(0));
//                    System.out.println(args.get(1));
//                    System.out.println(args.get(2));

                    /////////////////////////
                    if (whdo == true) {

                        child = child_temp;
                        //args_temp.add(convert(cell, child));
                        //  System.out.println(child.getNumberOfChildren());
                        args_temp.add(convert(cell, child));
                        Expression fi = null;
                        try {
                            //  while (args.get(0).evaluate().toBoolean() != false) {
//                            while ((child = child.getNextSibling()) != null) {
                            for (int i = 0; i < 2; i++) {
                                fi = convert(cell, child);
                                args_temp.add(fi);
                                if (child.getFirstChild() != null) {
                                    child = child.getNextSibling().getFirstChild();
                                }
                                //System.out.println(convert(cell,child));

                                // a++;
                                // System.out.println(child.toString());
                                //   convert(cell,child);
                                //   System.out.println("Teste:"+convert(cell,child));
                                //   }
//                                if ((child.getNextSibling()==null) &&(args.get(0).evaluate().toBoolean() != false))
//                                    child = child_temp;


                            }

//                                                for (int i = 0; i < args_temp.size(); i++) {
//                                                    System.out.println(args_temp.get(i));
//
//                                  
                            // System.out.println(args_temp.size());
                            //   System.out.println(a);   
                            //System.out.println(args_temp.toString());
                            //convert(cell,child);
                            //  }




                        } catch (Exception e) {
                        }
                    }
                } else if (function.getIdentifier() == "DW") {
                    //System.out.println("DoWhile!");
                    /////////////////////////////////////////////////////////////////////////////////////////////////////
                    try {

                        args.add(0, convert(cell, child));
                        args.add(1, convert(cell, child.getNextSibling()));
                        args.add(2, convert(cell, child.getNextSibling().getNextSibling()));

//                      System.out.println(convert(cell,child));   
//                      System.out.println(convert(cell,child.getNextSibling()));
//                      //child = child.getFirstChild();
//                      System.out.println(convert(cell,child.getNextSibling().getNextSibling()));
//
//                      System.out.println(convert(cell,child.getFirstChild().getFirstChild()));

//                      System.out.println(convert(cell,child.getNextSibling()).evaluate());   
//                      System.out.println(convert(cell,child.getNextSibling().getNextSibling()).evaluate());   


                        if (args.get(2).evaluate().toBoolean() != false) {
                            dowh = true;

                            while (args.get(2).evaluate().toBoolean() != false) {

                                while (((child = child.getNextSibling()) != null) || (child.getFirstChild() != null)) {
                                    //  for (int i = 0; i < 2; i++) {
                                    //  if(args.get(0).evaluate().toBoolean() != false){
                                    //System.out.println(args.get(0).evaluate().toString());
                                    //System.out.println(convert(cell,child));
                                    args.add(convert(cell, child));
                                    //a++;


                                    //   }
                                    //convert(cell,child);


                                    // convert(cell,child);
                                    //   convert(cell,child);
                                    //   System.out.println("Teste:"+convert(cell,child));
                                    //   }
                                    if ((child.getNextSibling() == null) && (args.get(2).evaluate().toBoolean() != false)) {
                                        child = child_temp;
//                                    System.out.println(child.getNextSibling().getFirstChild().getNextSibling());
                                    }
                                }

                                //convert(cell,child);
                            }

                        } else {
                            System.out.println("Condição falsa! Apenas uma iteração foi realizada");
                        }
                    } catch (Exception e) {
                        //System.out.println(e);
                    }
//                    System.out.println(args.get(0));
//                    System.out.println(args.get(1));
//                    System.out.println(args.get(2));

                    if (dowh == true) {
                        //System.out.println("DoWhile!"); 
                        child = child_temp;
                        //args_temp.add(convert(cell, child));
                        System.out.println(child.getNumberOfChildren());
                        args_temp.add(convert(cell, child));
                        Expression fi = null;
                        try {
                            //  while (args.get(0).evaluate().toBoolean() != false) {
//                            while ((child = child.getNextSibling()) != null) {
                            //for (int i = 0; i <= 2; i++) {
                            while (((child = child.getNextSibling()) != null) || (child.getFirstChild() != null)) {

                                fi = convert(cell, child);
                                args_temp.add(fi);

                                if ((child.getNextSibling() == null) && (args.get(0).evaluate().toBoolean() != false)) {
                                    child = child_temp;
                                }
                            }


                        } catch (Exception e) {
                        }
                    } else {
                        try {
                            while (((child.getNextSibling()) != null) && (args_temp.get(0).evaluate().toBoolean() != false)) {
                                args_temp.add(convert(cell, child));
                                child = child.getNextSibling();
                            }

                        } catch (Exception e) {
                        }
                        //args_temp.add(convert(cell,child.ge));
                    }


                } else {
                    args_temp.add(convert(cell, child));
                    while ((child_temp = child_temp.getNextSibling()) != null) {
                        args_temp.add(convert(cell, child_temp));
                    }

                }

            }

//            System.out.println(args_temp.get(0).toString());
//            System.out.println(args_temp.get(1).toString());
//            System.out.println(args_temp.get(2).toString());

            Expression[] argArray = args_temp.toArray(new Expression[args_temp.size()]);

            // System.out.println("0: "+argArray[0].toString());
            FunctionCall fc = new FunctionCall(function, argArray);
//                        try{
//                        while(argArray[0].evaluate().toBoolean()==!false){
//                            //fc = new FunctionCall(function,argArray);
//
//                        }
//                        
//                        }
//                        catch(Exception e){
//                        
//                        
//                        }
//         

            return fc;

        } else //System.out.println(node.getNumberOfChildren());
        {
            if (node.getNumberOfChildren() == 1) // Convert unary operation
            {
                return goUnaryOperator(cell, node);
            } else if (node.getNumberOfChildren() == 2) {
//            BinaryOperator operator = Language.getInstance().getBinaryOperator(node.getText());
                if (node.getType() == NewFormulaParserTokenTypes.SEMI) {
                    //left node
                    convert(cell, node.getFirstChild());
                    //right node
                    return convert(cell, node.getFirstChild().getNextSibling());
                } else {
                    //if(function.getIdentifier()=="WD")
                    //System.out.println(node.getText());
                }


                //System.out.println(node.getText());
                return goBinaryOperator(cell, node);


            } // Não é suposto ser else!
            else {
                //System.out.println("TESTE Exception:"+node.getNextSibling().getNextSibling());
                throw new FormulaCompilationException();
            }
        }
    }

    /**
     * Creates a binaryOperation from a cell and a AST node
     *
     * @param cell The current cell
     * @param node the current node from the Abstract Syntax Tree
     * @return UnaryOperation
     */
    private UnaryOperation goUnaryOperator(Cell cell, AST node) throws FormulaCompilationException {
        return new UnaryOperation(
                Language.getInstance().getUnaryOperator(node.getText()),
                convert(cell, node.getFirstChild()));
    }

    /**
     * Creates a new binary operator and corresponding binaryOperation
     *
     * @param cell The current cell
     * @param node the current node from the Abstract Syntax Tree
     * @return BinaryOperation
     */
    private BinaryOperation goBinaryOperator(Cell cell, AST node) throws FormulaCompilationException {
        // Convert binary operation
        BinaryOperator operator = null;
        //System.out.println(node.getFirstChild().getText());
//       if (node.getFirstChild().getType()==NewFormulaParserTokenTypes.WD)
//            System.out.println("A entrar no WhileDo...");
        //op = Language.getInstance().getFunction("whiledo");

        // System.out.println(Language.getInstance().getFunction("WhileDo").toString());
        //System.out.println(node.getType());

        try {
            operator = Language.getInstance().getBinaryOperator(node.getText());
            //System.out.println(node.getText());
        } catch (Exception e) {
        }





        if (operator instanceof RangeReference) {
            return new ReferenceOperation(
                    (Reference) convert(cell, node.getFirstChild()),
                    (RangeReference) operator,
                    (Reference) convert(cell, node.getFirstChild().getNextSibling()));
        } else {
            if (operator instanceof Attribution) {
                //System.out.println("Atribuicao");
                if (node.getFirstChild().getType() == NewFormulaParserTokenTypes.TEMPVAR) {
                    setTempVar(cell, node);
                } else {
                    //calling of the method to update the cell content
                    updateCellContent(cell, node);
                }
            }
        }

        return new BinaryOperation(
                convert(cell, node.getFirstChild()),
                operator,
                convert(cell, node.getFirstChild().getNextSibling()));
    }

    /**
     * Update's a cell content
     *
     * @param cell The current cell
     * @param node the current node from the Abstract Syntax Tree
     */
    private void updateCellContent(Cell cell, AST node) throws FormulaCompilationException {
        try {
            try {
                try {
                    CellReference cell_ref = new CellReference(cell.getSpreadsheet(), node.getFirstChild().getText());
                    Cell cell_atr = cell_ref.getCell();
                    AST child = node.getFirstChild();
                    Expression expression = convert(cell_atr, child.getNextSibling());
                    Value value = expression.evaluate();
                    UpdateCell.getInstance().updateCellContent(cell_atr, value);
                } catch (ParseException e) {
                    throw new FormulaCompilationException(e);
                }
            } catch (FormulaCompilationException e) {
                throw new FormulaCompilationException(e);
            }
        } catch (IllegalValueTypeException e) {
            throw new FormulaCompilationException(e);
        }
    }

    /**
     * Gets the Expression representing the temporary variable
     *
     * @param node the current node from the Abstract Syntax Tree
     * @return String with the concatenated nodes
     */
    private Expression getTempVarExpression(AST node) throws FormulaCompilationException {
        TempVarRepository instance = TempVarRepository.getInstance();
        TempVarReference tempVar = instance.getTempVarReference(node.toString());
        if (tempVar != null) { //Diferente not null
            Value value = tempVar.getValue();
            if (value.getType() == Value.Type.NUMERIC) {
                return new Literal(value);
            } else {
                return new Literal(Value.parseValue(value.toString(), Value.Type.BOOLEAN, Value.Type.DATE));
            }
        } else {
            throw new FormulaCompilationException(node.getText() + " not found");
        }
    }

    private void setTempVar(Cell cell, AST node) throws FormulaCompilationException {
        Expression expression = convert(cell, node.getFirstChild().getNextSibling());
        Value value;
        try {
            value = expression.evaluate();
            TempVarReference tempVarRef = new TempVarReference(node.getFirstChild().getText(), value);
            TempVarRepository.getInstance().addTempVarReference(tempVarRef);
        } catch (IllegalValueTypeException ex) {
            throw new FormulaCompilationException(ex);
        }
    }

    private void emptyTempVarStorage() {
        TempVarRepository.getInstance().emptyTempVarRepository();
    }

    class SequenceThreadExecuter implements Runnable {

        private Cell cell;
        private AST node;
        Expression expression;
        String message;

        public SequenceThreadExecuter(Cell cell, AST node) {
            this.cell = cell;
            this.node = node;
        }

        @Override
        public void run() {
            try {
                //System.out.println("Thread executing...");
                expression = convert(cell, node);
            } catch (Exception ex) {
                message = ex.getMessage();
            }
        }

        public Expression getExpression() throws Exception {
            if (expression == null) {
                throw new FormulaCompilationException(message);
            }
            return expression;
        }
    }
}