package csheets.ext.macro;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Spreadsheet;
import csheets.core.Workbook;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.CardinalExpressionCompiler;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.lang.CellReference;
import csheets.core.formula.newCompiler.NewFormulaLexer;
import csheets.core.formula.newCompiler.NewFormulaParser;
import csheets.ui.ctrl.UIController;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;

/**
 *
 * @author Fábio Queirós
 */
public class ControllerMacro {

    private UIController uiCtrl;
    private MacroRepository macroRep;

    /**
     * Creat this controller
     *
     * @param uiCtrl
     */
    public ControllerMacro(UIController uiCtrl) {
        this.uiCtrl = uiCtrl;
        this.macroRep = new MacroRepository();
    }

    /**
     * add a macro in the repository
     *
     * @param macro
     */
    public void addMacro(Macro macro) {
        macroRep.addMacro(macro);
    }

    /**
     * return a macro finded by name
     *
     * @return name
     */
    public Macro getMacroGivenAName(String name) {
        return macroRep.getMacroGivenAName(name);
    }

    /**
     * edite a macro sending a new macro
     *
     * @param m
     */
    public void editMacro(Macro m) {
        macroRep.editMacro(m);
    }

    /**
     * return a vector with all macro names
     *
     * @return vector of macros name
     */
    public String[] allMacroNamesVector() {
        return macroRep.allMacroNamesVector();
    }

    /**
     * Return the repository of macros
     *
     * @return macros
     */
    public ArrayList<Macro> getMacros() {
        return macroRep.getMacros();
    }

    /**
     * Return this controller
     *
     * @return uictrl
     */
    public UIController getUiCtrl() {
        return uiCtrl;
    }

    /**
     * this method iniciat the execution of a macro
     *
     * @param macro
     */
    public void execMacro(Macro macro) {
        MacroExecuter me = new MacroExecuter(this, macro);
        me.start();
    }

    /**
     * this method run all expression of a macro and return a string output with
     * all expressions and the values
     *
     * @param output
     * @param macro
     * @return output
     * @throws FormulaCompilationException
     * @throws IllegalValueTypeException
     * @throws ParseException
     */
    public String runMacro(String output, Macro macro) throws FormulaCompilationException, IllegalValueTypeException, ParseException {
        Macro newMacro;
        String macroName;
        String[] pa;
        String param;
        for (int i = 0; i < macro.getExpressions().size(); i++) {
            macroName = macro.getExpressions().get(i).toString();
            if (macroName.startsWith("exec")) {
                newMacro = macroRep.getMacroGivenAName(macroName.split(",")[0].replace("exec(", ""));
                macroName = macroName.replace("exec(", "");
                macroName = macroName.replace(")", "");
                pa = macroName.split(",");
                param = "";
                for (int k = 1; k < pa.length; k++) {
                    param = param + "$" + k + "=" + pa[k] + ",";
                }
                newMacro.setParam(param);
                output = newMacro.getName() + "\n" + runMacro(output, newMacro);

            } else {
                output = output + "\n" + runMacroLine(macro.getExpressions().get(i).toString(), macro.getParam());
            }
        }
        return output;
    }

    /**
     * this method compile an expression of the macro and return the expression
     * and the value
     *
     * @param line
     * @param para
     * @return output
     * @throws ParseException
     * @throws FormulaCompilationException
     * @throws IllegalValueTypeException
     */
    public String runMacroLine(String line, String para) throws ParseException, FormulaCompilationException, IllegalValueTypeException {
        String output = "";
        String var = "";
        String exp = "";
        Workbook workbook = new Workbook(1);
        Spreadsheet sheet = workbook.getSpreadsheet(0);
        String cellFake = "A1";
        CellReference cr = new CellReference(this.getUiCtrl().getActiveSpreadsheet(), cellFake);
        Cell cell = cr.getCell();
        CardinalExpressionCompiler compiler = new CardinalExpressionCompiler();
        NewFormulaParser parser;
        Expression expression;
        String lineTeste = line.split(":=")[1];
        String[] param = para.split(",");

        if (lineTeste.matches("[$][0-9]*")) {
            for (int i = 0; i < param.length; i++) {
                if (param[i].startsWith(lineTeste)) {
                    line = line.split(":=")[0] + ":=" + param[i].split("=")[1];
                }
            }
        }
        line = "#" + line;
        parser = new NewFormulaParser(new NewFormulaLexer(new StringReader(line)));
        expression = compiler.compile(cell, line);
        output = line + "<" + expression.evaluate().toString();
        if (output.startsWith("#$")) {
            var = "\nTemp Var: " + output.split("<")[0] + "  Result: " + output.split("<")[1];
            var = var.replace('$', ' ');
            var = var.replace('#', ' ');
        } else {
            exp = "\nExpression: " + output.split("<")[0] + "  Result: " + output.split("<")[1];
            exp = exp.replace('#', ' ');
        }
        output = exp + var;
        return output;
    }

    /**
     * this method verify if exist a macro whit the same name
     *
     * @param name
     * @return boolean
     */
    public boolean verifyNameMacro(String name) {
        String macros[] = macroRep.allMacroNamesVector();
        for (int i = 0; i < macros.length; i++) {
            if (macros[i].trim().equals(name.trim())) {
                return false;
            }
        }
        return true;
    }
}
