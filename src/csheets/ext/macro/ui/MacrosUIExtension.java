package csheets.ext.macro.ui;

import csheets.ext.Extension;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;
import javax.swing.Icon;
import javax.swing.JComponent;

public class MacrosUIExtension extends UIExtension {

    private Icon icon;
    private JComponent sideBar;

    public MacrosUIExtension(Extension extension, UIController uiController) {
        super(extension, uiController);
    }

    public Icon getIcon() {
        return null;
    }
    
    public JComponent getSideBar() 
        {
		if(sideBar==null)
                {
                    sideBar=new MacroUI(uiController);
                }
                return sideBar;
        }
}
