/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.persistencia;

import csheets.core.Workbook;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Calendar;
import javax.sql.rowset.serial.SerialBlob;
/**
 *
 * @author Tiago
 */
public class XMLMap implements Serializable
{
    private int id;
    private java.sql.Timestamp time;
    private Blob blob;
    
    public XMLMap()
    {
        id=0;
        time=null;
        blob=null;
    }
    public XMLMap (Workbook workbook) throws IOException, SQLException
    {
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        time = new java.sql.Timestamp(now.getTime());
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(); 
        ObjectOutputStream objetStream = new ObjectOutputStream(byteStream); 
        objetStream.writeObject(workbook); 
        objetStream.flush();
        objetStream.close();
        byteStream.close();
        byte[] b = byteStream.toByteArray();
        blob = new SerialBlob(b);
    }
    
    public void setBlob(Blob b)
    {
        blob = b;
    }
    
    public Blob getBlob()
    {
        return blob;
    }
    
    public void setId(int i)
    {
        id = i;
    }
    public int getId()
    {
        return id;
    }
    
    public void setTime(java.sql.Timestamp time)
    {
        this.time = time;
    }
    public java.sql.Timestamp getTime() 
    {
        return time;
    }
}
