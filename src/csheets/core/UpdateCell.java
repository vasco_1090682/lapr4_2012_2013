package csheets.core;

import csheets.core.formula.compiler.FormulaCompilationException;
import java.text.NumberFormat;

/**
 *
 * @author Fábio Queirós
 */
public class UpdateCell {
    
        //singleton
        private static UpdateCell instance = new UpdateCell();
        
        //empty 
        private void UpdateCellContent(){
        }
        
     /**
     * Update's a cell content with a given value
     * @param cell the current cell of the spreadsheet
     * @param value the value to be assigned to the cell 
     */
        public void updateCellContent(Cell cell, Value value) throws FormulaCompilationException{
                if(value.getType() == Value.Type.NUMERIC)
                    cell.setContent(value.toString(NumberFormat.getInstance()));
                else
                    cell.setContent(value.toString());
        }
        
        public static UpdateCell getInstance(){
            return instance;
        }     
}
