package csheets.ext.macro.ui;

import csheets.ext.macro.*;
import csheets.ui.ctrl.UIController;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MacroUI extends JPanel {

    private static JComboBox<String> cmbMacrosNames;
    private JList<Object> listExpressions;
    private JButton btRun, btNew, btEdit;
    String[] macrosNames;
    private static ControllerMacro ctrlMacro;
    DefaultComboBoxModel<String> model;

    public MacroUI(UIController uiController) {
        super();
        setName(MacrosExtension.NAME);
        ctrlMacro = new ControllerMacro(uiController);
        BootstrapMacro macross = new BootstrapMacro();
        macross.createMacros(ctrlMacro);

        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lbName = new JLabel("Name:");
        p1.add(lbName);

//        macrosNames = ctrlMacro.allMacroNamesVector();
        model = new DefaultComboBoxModel<String>(ctrlMacro.allMacroNamesVector());
        cmbMacrosNames = new JComboBox<String>(model);
        cmbMacrosNames.setSelectedIndex(-1);
        cmbMacrosNames.setPreferredSize(new Dimension(200, 25));
        p1.add(cmbMacrosNames);

        listExpressions = new JList<Object>();
        listExpressions.setFixedCellWidth(300);
        listExpressions.setEnabled(true);
        JScrollPane scrollPane = new JScrollPane(listExpressions);

        //buttons
        JPanel p2 = new JPanel();
        btRun = new JButton("Run");
        btNew = new JButton("New");
        btEdit = new JButton("Edit");
        p2.add(btRun);
        p2.add(btNew);
        p2.add(btEdit);

        p.add(p1, BorderLayout.NORTH);
        p.add(scrollPane, BorderLayout.CENTER);
        p.add(p2, BorderLayout.SOUTH);

        add(p);

        ProcessEvent pe = new ProcessEvent();
        cmbMacrosNames.addActionListener(pe);
        btRun.addActionListener(pe);
        btNew.addActionListener(pe);
        btEdit.addActionListener(pe);
    }

    public static void setComboBoxIndex(int index){
        cmbMacrosNames.setSelectedIndex(index);
    }
    
    private class ProcessEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == cmbMacrosNames) {
                String name = (String) (cmbMacrosNames.getSelectedItem());
                Macro macro = ctrlMacro.getMacroGivenAName(name);
                
                listExpressions.setListData(macro.getExpressions().toArray());
            } else if (e.getSource() == btRun) {
                String name = (String) (cmbMacrosNames.getSelectedItem());
                if (name != null) {
                    Macro macro = ctrlMacro.getMacroGivenAName(name);
                    macro.setParam("");
                    ctrlMacro.execMacro(macro);
                } else {
                    JOptionPane.showMessageDialog(null, "Select a macro", "Warning", JOptionPane.WARNING_MESSAGE);
                }
            } else if (e.getSource() == btNew) {
                Macro macro = null;
                macroNewUI m = new macroNewUI(null, "Nova macro", macro, ctrlMacro, model);
            } else if (e.getSource() == btEdit) {
                Macro macro = ctrlMacro.getMacroGivenAName((String)cmbMacrosNames.getSelectedItem());
                int index = cmbMacrosNames.getSelectedIndex();
                EditMacroUI editMacro = new EditMacroUI(macro, ctrlMacro, index);
                editMacro.setVisible(true);
            } 
        }
    }
}
