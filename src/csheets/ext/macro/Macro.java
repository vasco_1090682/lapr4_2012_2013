package csheets.ext.macro;

import java.util.ArrayList;

/**
 *
 * @author Fábio Queirós
 */

public class Macro {

    private String name;
    private ArrayList<String> expressions;
    private String param;
    

    /**
     * constructer of macro
     */
    public Macro(){
        name = null;
        expressions = null;
        param = "";
    }
    
    /**
     * Constructer with params of a macro
     * @param name
     * @param param
     * @param expressions
     */
    public Macro(String name, String param, ArrayList<String> expressions){
        this.name = name;
        this.param = param;
        this.expressions = expressions;
    }
    /**
     * Constructer copy of a macro
     * @param m
     */
    public Macro(Macro m){
        this.expressions = m.getExpressions();
        this.name = m.getName();
        this.param = m.getParam();
    }

    /**
     * return the name of macro
     * @return name
     */
    public String getName(){
        return name;
    }
    
    /**
     * return the ArrayList of expressions of the macro
     * @return expressions
     */
    public ArrayList<String> getExpressions(){
        return expressions;
    }

    /**
     * return the name and the expressions of the macro
     * @return
     */
    public String toString(){
        return name + "\n" + expressions.toString();
    }

    /**
     * Return the String of the params of the macro
     * @return param
     */
    public String getParam() {
        return param;
    }

    /**
     * @param param the param to set
     */
    public void setParam(String param) {
        this.param = param;
    }
}
