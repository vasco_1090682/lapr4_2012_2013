/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.Value;

/**
 *
 * @author Fábio Queirós
 */
public class TempVarReference {
    
    private String name;
    private Value value;
    
    public TempVarReference(String name, Value value){
        this.name = name;
        this.value = value;
    }
    
    public void setValue(Value value){
        this.value = value;
    }
    
    public Value getValue(){
        return value;
    }
    
    public String getname(){
        return name;
    }
    
}