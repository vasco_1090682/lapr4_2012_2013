/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.Value.Type;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;

/**
 *
 * @author Fábio Queirós
 */
public class Attribution implements BinaryOperator {
    
    public Attribution(){
        
    }

    @Override
    public Value applyTo(Expression leftOperand, Expression rightOperand) throws IllegalValueTypeException {
        return rightOperand.evaluate();
    }

    @Override
    public String getIdentifier() {
        return ":=";
    }

    @Override
    public Type getOperandValueType() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
