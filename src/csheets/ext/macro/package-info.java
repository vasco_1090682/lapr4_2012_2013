/**
 * <p> <b>Diagrams</b>
 * <p>
 * <b>Sequence Diagram</b>
 * <p>
 * <img src="doc-files/IT1_Macros_Trilho4_Sequencia1.png">
 * <p>
 * <p>
 * <img src="doc-files/IT1_Macros_Trilho4_Sequencia2.png">
 * <p>
 * <b>Class Diagram</b>
 * <b>
 * <img src="doc-files/IT1_Macros_Trilho4_Classes.png">
 * <p>
 * <b>Use Case</b>
 * <img src="doc-files/IT1_Macros_Trilho4_UseCase.png">
 * <p>
 * <b>Class Diagram 2</b>
 * <b>
 * <img src="doc-files/IT3_Macros_Trilho4_Classes2.png">
 * <p>
 * 
 * @author Vasco Ribeiro
 * 
 */
/*
 * @startuml doc-files/IT1_Macros_Trilho4_Sequencia1.png
 * participant "User"
 * participant "MacroUI"
 * participant "MacroNewUI"
 * participant "ControllerMacro"
 * 
 * User -> MacroUI : choose option "New"
 * 
 * MacroUI -> MacroNewUI : <<create>>
 * activate MacroNewUI
 * MacroUI -> MacroNewUI : enter name
 * MacroUI -> MacroNewUI : enter expressions
 * 
 * MacroNewUI -> ControllerMacro : addMacro(macro)
 * activate ControllerMacro
 * ControllerMacro --> MacroNewUI : <<added>>
 * deactivate ControllerMacro
 * 
 * MacroNewUI --> MacroUI : <<close>>
 * deactivate MacroNewUI
 * @enduml
 * 
 * @startuml doc-files/IT1_Macros_Trilho4_Sequencia2.png
 * participant "User"
 * participant "MacroUI"
 * participant "ControllerMacro"
 * participant "MacroExecuter"
 * participant "cr:CellReference" as cr
 * participant "Cell"
 * participant "FormulaCompiler"
 * 
 * User -> MacroUI : choose option "Run" selected macro
 * 
 * MacroUI -> ControllerMacro : execMacro(macro)
 * ControllerMacro -> MacroExecuter : runMacro(macro)
 * activate MacroExecuter
 * loop for each expression
 *      MacroExecuter -> MacroExecuter : executeExpression(cellAddress, formula)
 *      MacroExecuter -> cr : new CellReference(Spreadsheet, cellAddress);
 *      MacroExecuter -> Cell : cr.getCell()
 *      MacroExecuter -> FormulaCompiler : getInstance()
 * end
 *      MacroExecuter --> ControllerMacro : <<end>>
 *      deactivate MacroExecuter
 * 
 * @enduml
 * 
 * @startuml doc-files/IT1_Macros_Trilho4_Classes.png
 * class Macro{
 *  - name : String
 *  - expressions : ArrayList<String>
 *  - Macro()
 *  - Macro(String name, ArrayList<String> expressions)
 *  - String getName()
 *  - ArrayList<String> getExpressions()
 *  - String toString()
 * }
 * class ControllerMacro{
 *  - macros : ArrayList<Macro>
 *  - uiCtrl : UIController
 *  - ControllerMacro(UIController uiCtrl)
 *  - addMacro(Macro macro)
 *  - ArrayList<Macro> getMacros()
 *  - UIController getUiCtrl()
 *  - String[] allMacroNamesVector()
 *  - Macro getMacroGivenAName(String name)
 *  - execMacro(Macro macro)
 * }
 * class MacroExecuter{
 *  - ctrlMacro : ControllerMacro
 *  - macro : Macro
 *  - MacroExecuter(ControllerMacro ctrlMacro, Macro macro)
 *  - run()
 *  - executeExpression(String cellAddress, String formula)
 *  - runMacro(Macro macro)
 * }
 * class FormulaCompiler{
 *  - FormulaCompiler : instance
 *  - properties_filename : String
 *  - List<ExpressionCompiler> compilers
 *  - FormulaCompiler()
 *  - FormulaCompiler getInstance()
 *  - Formula compile(Cell cell, String source)()
 * }
 * class CellReference{
 *  - PATTERN : Pattern
 *  - ABSOLUTE_OPERATOR : String
 *  - cell : Cell
 *  - columnAbsolute : boolean
 *  - rowAbsolute : boolean
 *  - CellReference(Cell cell)
 *  - CellReference(Cell cell, boolean columnAbsolute, boolean rowAbsolute)
 *  - CellReference(Spreadsheet spreadsheet, String reference)
 *  - Value evaluate()
 *  - Object accept(ExpressionVisitor visitor)
 *  - Cell getCell()
 *  - SortedSet<Cell> getCells()
 *  - boolean isColumnAbsolute()
 *  - boolean isRowAbsolute()
 *  - int compareTo(Reference reference)
 *  - String toString()
 * }
 * class Cell{
 *  - Spreadsheet getSpreadsheet()
 *  - Address getAddress()
 *  - Value getValue()
 *  - String getContent()
 *  - Formula getFormula()
 *  - setContent(String content)
 *  - clear()
 *  - SortedSet<Cell> getPrecedents()
 *  - SortedSet<Cell> getDependents()
 *  - copyFrom(Cell source)
 *  - moveFrom(Cell source)
 *  - addCellListener(CellListener listener)
 *  - removeCellListener(CellListener listener)
 *  - CellListener[] getCellListeners()
 * }
 * 
 * Macro - ControllerMacro
 * Macro - MacroExecuter
 * ControllerMacro <- MacroExecuter
 * MacroExecuter <- FormulaCompiler
 * MacroExecuter <- CellReference
 * MacroExecuter <- Cell
 * @enduml
 * 
 * @startuml doc-files/IT1_Macros_Trilho4_UseCase.png
 * 
 * User -> (Add macro)
 * User -> (Run macro)
 * 
 * @enduml
 * 
 * @startuml doc-files/IT3_Macros_Trilho4_Classes2.png
 * class Macro{
 *  - name : String
 *  - expressions : ArrayList<String>
 *  - param : String
 *  - Macro()
 *  - Macro(String name, ArrayList<String> expressions)
 *  - Macro(String name, ArrayList<String> expressions, String param)
 *  - String getName()
 *  - String getParam()
 *  - setParam(String param)
 *  - ArrayList<String> getExpressions()
 *  - String toString()
 * }
 * class ControllerMacro{
 *  - uiCtrl : UIController
 *  - ControllerMacro(UIController uiCtrl)
 *  - addMacro(Macro macro)
 *  - ArrayList<Macro> getMacros()
 *  - UIController getUiCtrl()
 *  - String[] allMacroNamesVector()
 *  - Macro getMacroGivenAName(String name)
 *  - execMacro(Macro macro)
 * }
 * class MacroRepository{
 *  - macro : Macro
 *  - addMacro(Macro macro)
 *  - ArrayList<Macro> getMacros()
 *  - String[] allMacroNamesVector(String name)
 *  - Macro getMacroGivenName(String name)
 * }
 * class MacroExecuter{
 *  - ctrlMacro : ControllerMacro
 *  - macro : Macro
 *  - MacroExecuter(ControllerMacro ctrlMacro, Macro macro)
 *  - run()
 *  - executeExpression(String cellAddress, String formula)
 *  - runMacro(Macro macro)
 * }
 * class FormulaCompiler{
 *  - FormulaCompiler instance
 *  - properties_filename : String
 *  - List<ExpressionCompiler> compilers
 *  - FormulaCompiler()
 *  - FormulaCompiler getInstance()
 *  - Formula compile(Cell cell, String source)()
 * }
 * class CellReference{
 *  - PATTERN : Pattern
 *  - ABSOLUTE_OPERATOR : String
 *  - cell : Cell
 *  - columnAbsolute : boolean
 *  - rowAbsolute : boolean
 *  - CellReference(Cell cell)
 *  - CellReference(Cell cell, boolean columnAbsolute, boolean rowAbsolute)
 *  - CellReference(Spreadsheet spreadsheet, String reference)
 *  - Value evaluate()
 *  - Object accept(ExpressionVisitor visitor)
 *  - Cell getCell()
 *  - SortedSet<Cell> getCells()
 *  - boolean isColumnAbsolute()
 *  - boolean isRowAbsolute()
 *  - int compareTo(Reference reference)
 *  - String toString()
 * }
 * class Cell{
 *  - Spreadsheet getSpreadsheet()
 *  - Address getAddress()
 *  - Value getValue()
 *  - String getContent()
 *  - Formula getFormula()
 *  - setContent(String content)
 *  - clear()
 *  - SortedSet<Cell> getPrecedents()
 *  - SortedSet<Cell> getDependents()
 *  - copyFrom(Cell source)
 *  - moveFrom(Cell source)
 *  - addCellListener(CellListener listener)
 *  - removeCellListener(CellListener listener)
 *  - CellListener[] getCellListeners()
 * }
 * class TempVarReference{
 *  - name : String
 *  - value : Value
 *  - TempVarReference(String name, Value value)
 *  - Value getValue()
 *  - setValue(Value value)
 *  - String getName()
 * }
 * class TempVarRepository{
 *  - instance : TempVarRepository
 *  - arrayTempVar : ArrayList<TempVarReference>
 *  - TempVarRepository getInstance()
 *  - addTempVarReference(TempVarReference tvf)
 *  - TempVarReference getTempVarReference(String name)
 *  - emptyTempVarRepository()
 * }
 * Macro - ControllerMacro
 * Macro - MacroExecuter
 * ControllerMacro <- MacroRepository
 * ControllerMacro <- MacroExecuter
 * MacroExecuter <- FormulaCompiler
 * FormulaCompiler <- TempVarReference
 * TempVarReference <- TempVarRepository
 * MacroExecuter <- CellReference
 * MacroExecuter <- Cell
 * @enduml
 */
package csheets.ext.macro;