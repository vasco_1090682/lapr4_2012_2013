package csheets.ext.partilha.controllers;

import csheets.ext.partilha.Partilha;
import csheets.ext.partilha.UDPClient;
import javax.swing.DefaultComboBoxModel;

/**
 * Controlador do cliente UDP
 * @author Vasco Ribeiro
 */
public class UDPClientController {
    
    private UDPClient udpclient;
    
    /**
     * Método que cria o cliente UDP
     * @param model Dados da JComboBox que contem informação sobre os servidor que anunciam na rede
     */
    public void create(DefaultComboBoxModel<String> model){
        udpclient = new UDPClient(model);
    }
    
    /**
     * Método que dá início à thread
     */
    public void start(){
        udpclient.start();
    }
    
    /**
     * Método que termina a thread
     */
    public void leaveServer(){
        udpclient.leaveServer();
    }
    
    /**
     * Dado um nome e área retorna a partilha correspondente
     * @param name Nome da partilha
     * @param area Area da partilha
     * @return Partilha que corresponde a uma nome e uma área
     */
    public Partilha getServer(String name, String area){
        return udpclient.getServer(name, area);
    }
}
