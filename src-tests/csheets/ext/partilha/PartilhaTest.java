/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.partilha;

import csheets.ext.partilha.Partilha.PermissionsTypes;
import java.net.InetAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vasco
 */
public class PartilhaTest {
    
    public PartilhaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Partilha.
     */
    @Test
    public void testGetName() throws PartilhaException {
        System.out.println("getName");
        Partilha instance = new Partilha("Lapr4", "A1:B5", "isep", PermissionsTypes.Write, true);
        String expResult = "Lapr4r";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getArea method, of class Partilha.
     */
    @Test
    public void testGetArea() throws PartilhaException {
        System.out.println("getArea");
        Partilha instance = new Partilha("Lapr4", "A1:B5", "isep", PermissionsTypes.Write, true);
        String expResult = "A1:B5";
        String result = instance.getArea();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPassword method, of class Partilha.
     */
    @Test
    public void testGetPassword() throws PartilhaException {
        System.out.println("getPassword");
        Partilha instance = new Partilha("Lapr4", "A1:B5", "isep", PermissionsTypes.Write, true);
        String expResult = "isep";
        String result = instance.getPassword();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPermission method, of class Partilha.
     */
    @Test
    public void testGetPermission() throws PartilhaException {
        System.out.println("getPermission");
        Partilha instance = new Partilha("Lapr4", "A1:B5", "isep", PermissionsTypes.Write, true);
        PermissionsTypes expResult = PermissionsTypes.Write;
        PermissionsTypes result = instance.getPermission();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getServerIp method, of class Partilha.
     */
    @Test
    public void testGetServerIp() {
        System.out.println("getServerIp");
        Partilha instance = null;
        InetAddress expResult = null;
        InetAddress result = instance.getServerIp();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isOnline method, of class Partilha.
     */
    @Test
    public void testIsOnline() throws PartilhaException {
        System.out.println("isOnline");
        Partilha instance = new Partilha("Lapr4", "A1:B5", "isep", PermissionsTypes.Write, true);
        boolean expResult = true;
        boolean result = instance.isOnline();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setName method, of class Partilha.
     */
    @Test
    public void testSetName() throws Exception {
        System.out.println("setName");
        String name = "";
        Partilha instance = null;
        instance.setName(name);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setArea method, of class Partilha.
     */
    @Test
    public void testSetArea() throws Exception {
        System.out.println("setArea");
        String area = "";
        Partilha instance = null;
        instance.setArea(area);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPassword method, of class Partilha.
     */
    @Test
    public void testSetPassword() throws Exception {
        System.out.println("setPassword");
        String password = "";
        Partilha instance = null;
        instance.setPassword(password);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPermission method, of class Partilha.
     */
    @Test
    public void testSetPermission() throws Exception {
        System.out.println("setPermission");
        PermissionsTypes permission = null;
        Partilha instance = null;
        instance.setPermission(permission);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setServerIp method, of class Partilha.
     */
    @Test
    public void testSetServerIp() {
        System.out.println("setServerIp");
        InetAddress serverIp = null;
        Partilha instance = null;
        instance.setServerIp(serverIp);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setOnline method, of class Partilha.
     */
    @Test
    public void testSetOnline() {
        System.out.println("setOnline");
        boolean online = false;
        Partilha instance = null;
        instance.setOnline(online);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
