/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.compiler;

import antlr.collections.AST;
import antlr.collections.List;
import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.core.Value;
import csheets.core.Workbook;
import csheets.core.formula.BinaryOperation;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionCall;
import csheets.core.formula.Literal;
import csheets.core.formula.lang.Language;
import csheets.core.formula.newCompiler.NewFormulaLexer;
import csheets.core.formula.newCompiler.NewFormulaParser;
import java.io.StringReader;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fábio Queirós
 */
public class CardinalExpressionCompilerTest {

    public CardinalExpressionCompilerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getStarter method, of class CardinalExpressionCompiler.
     */
    @Test
    public void testGetStarter() {
        System.out.println("getStarter");
        CardinalExpressionCompiler instance = new CardinalExpressionCompiler();
        char expResult = '#';
        char result = instance.getStarter();
        assertEquals(expResult, result);
    }

    /**
     * Test of compile method, of class CardinalExpressionCompiler.
     */
    @Test
    public void testCompile() throws Exception {
        Workbook workbook = new Workbook(1);
        Spreadsheet sheet = workbook.getSpreadsheet(0);
        Cell cell = sheet.getCell(new Address(0, 0));

        CardinalExpressionCompiler compiler = new CardinalExpressionCompiler();
        String line = "#sum(2+2)";
        NewFormulaParser parser = new NewFormulaParser(new NewFormulaLexer(new StringReader(line)));
        parser.expression();

        AST ast = parser.getAST();
        Function function = Language.getInstance().getFunction(ast.getText());
        ArrayList args = new ArrayList<Expression>();


        //getting string of the operator
        AST child = ast.getFirstChild();
        BinaryOperator operator = Language.getInstance().getBinaryOperator(child.getText());

        //getting 1st number from ast
        AST n1 = child.getFirstChild();
        //getting 2nd number from ast    
        AST n2 = n1.getNextSibling();

        BinaryOperation op = new BinaryOperation(new Literal(Value.parseNumericValue(n1.getText())), operator, new Literal(Value.parseNumericValue(n2.getText())));
        args.add(op);

        Expression[] argArray = (Expression[]) args.toArray(new Expression[args.size()]);
        FunctionCall expResult = new FunctionCall(function, argArray);

        //resultado esperado
        Expression result = compiler.compile(cell, line);

        assertEquals(expResult.toString(), ((FunctionCall) result).toString());

    }

    /**
     * Test of convert method, of class CardinalExpressionCompiler.
     */
    @Test
    public void testConvert() throws Exception {
        System.out.println("Convert");
        Workbook workbook = new Workbook(1);
        Spreadsheet sheet = workbook.getSpreadsheet(0);
        Cell cell = sheet.getCell(new Address(0, 0));
        CardinalExpressionCompiler compiler = new CardinalExpressionCompiler();
        String line = "#A1:=sum(2+5)";
        NewFormulaParser parser = new NewFormulaParser(new NewFormulaLexer(new StringReader(line)));
        parser.expression();
        AST ast = parser.getAST();
        Expression result = compiler.convert(cell, ast);
        String result_str = result.evaluate().toString();
        assertEquals(result_str, "7");
        Cell cell_atr = sheet.getCell(new Address(0, 0));
        assertEquals(cell_atr.getContent().toString(), "7");

    }
}
