/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.macro;

import java.util.ArrayList;

/**
 *
 * @author Fábio Queirós
 */
public class MacroRepository {

    private ArrayList<Macro> macros = new ArrayList<Macro>();

    /**
     * Add a macro to repository
     *
     * @param macro
     */
    public void addMacro(Macro macro) {
        macros.add(macro);
    }
    
        /**
     * Return the repository of macros
     *
     * @return macros
     */
    public ArrayList<Macro> getMacros() {
        return macros;
    }
    
        /**
     * return all names of macros in a String vector
     *
     * @return names
     */
    public String[] allMacroNamesVector() {
        String[] names = new String[macros.size()];
        {
            for (int i = 0; i < names.length; i++) {
                names[i] = macros.get(i).getName();

            }
        }
        return names;
    }
    
        /**
     * this method return a macro finded by a
     *
     * @param name
     * @param name
     * @return macro
     */
    public Macro getMacroGivenAName(String name) {
        Macro macro = new Macro();
        for (Macro m : macros) {
            if (m.getName().equals(name)) {
                macro = m;
            }
        }
        return macro;
    }
    
        /**
     * find teh macro and replace the macro for the new edited macro
     *
     * @param macro
     */
    public void editMacro(Macro macro) {
        for (int i = 0; i < macros.size(); i++) {
            if (macro.getName().equals(macros.get(i).getName())) {
                macros.remove(i);
            }
        }
        macros.add(macro);
    }
}
