/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.partilha.ui;

import csheets.ext.partilha.ExtensionPartilha;
import csheets.ext.partilha.PartilhaClient;
import csheets.ext.partilha.PartilhaServer;
import csheets.ext.partilha.UDPClient;
import csheets.ext.partilha.UDPServer;
import csheets.ui.ctrl.UIController;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Tiago
 */
public class PartilhaPanel extends JPanel {

    private JPanel panel1 = new JPanel(), panel2 = new JPanel(), panel3 = new JPanel(), panel4 = new JPanel(), panel5 = new JPanel(), panel6 = new JPanel(), panel7 = new JPanel(), panel8 = new JPanel();
    private JLabel label1 = new JLabel(), label2 = new JLabel(), label3 = new JLabel(), label4 = new JLabel(), label5 = new JLabel(), label6 = new JLabel(), label7 = new JLabel(), label8 = new JLabel();
    private JTextField textField1 = new JTextField(), textField2 = new JTextField(), textField3 = new JTextField(), textField4 = new JTextField(), textField5 = new JTextField();
    private JButton button1 = new JButton(), button2 = new JButton(), button3 = new JButton();
    private UIController uiController;
    private PartilhaServer server;

    public PartilhaPanel(UIController ui) {
        super(new GridLayout(14, 0));
        setName(ExtensionPartilha.NAME);
        uiController = ui;

        label1.setText("Criar Partilha");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
        add(label1);

        panel1.setLayout(new GridLayout(1, 2));

        label2.setText("Area: ");
        panel1.add(label2);

        textField1.setText("A1:B2");
        panel1.add(textField1);

        add(panel1);

        panel2.setLayout(new GridLayout(1, 2));

        label3.setText("Porta:");
        textField2.setText("500");
        panel2.add(label3);
        panel2.add(textField2);

        add(panel2);
        add(label4);

        panel3.setLayout(new GridLayout(1, 2));

        button1.setText("Criar Partilha");
        button1.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (textField1.getText().split(":").length == 2) {
                            if (textField1.getText().split(":")[0].matches("[A-Z][0-9]*") && textField1.getText().split(":")[1].matches("[A-Z][0-9]*")) {
                                if (!textField2.getText().isEmpty() && textField2.getText().matches("[0-9]*")) {
//                                    server = new PartilhaServer(textField1.getText(), uiController);
                                    server.start();
//                                    UDPServer udpserver = new UDPServer();
//                                    udpserver.start();
                                    button2.setVisible(true);
                                    JOptionPane.showMessageDialog(null, "Partilha Criada");
                                } else {
                                    JOptionPane.showMessageDialog(null, "Verifique o valor da Porta");
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Células não inseridas correctamente\n Ex: A1:B2");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Area não inserida correctamente");
                        }
                        button1.setEnabled(false);

                    }
                });
        panel3.add(button1);

        button2.setText("Parar Partilha");
        button2.setVisible(false);
        button2.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (server.isAlive()) {
                            server.stopServer();
                            button2.setVisible(false);
                            button1.setEnabled(true);
                        }
                    }
                });
        panel3.add(button2);

        add(panel3);

        add(panel4);

        label5.setText("Aceder a Partilha");
        label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
        add(label5);

        panel5.setLayout(new GridLayout(1, 2));
        label6.setText("Area de destino:");
        textField3.setText("A1:B2");
        panel5.add(label6);
        panel5.add(textField3);
        add(panel5);

        panel6.setLayout(new GridLayout(1, 2));
        label7.setText("IP:");
        textField4.setText("192.168.0.195");
        panel6.add(label7);
        panel6.add(textField4);
        add(panel6);

        panel7.setLayout(new GridLayout(1, 2));
        label8.setText("Porta:");
        textField5.setText("500");
        panel7.add(label8);
        panel7.add(textField5);
        add(panel7);

        add(panel8);

        button3.setText("Aceder a Partilha");
        button3.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (textField3.getText().split(":")[0].matches("[A-Z][0-9]*") && textField3.getText().split(":")[1].matches("[A-Z][0-9]*")) {
                            /*
                             * validacao ip (consultado em 02/06/2013): http://www.mkyong.com/regular-expressions/how-to-validate-ip-address-with-regular-expression/
                             */
                            if (textField4.getText().split("\\.").length == 4 && textField4.getText().matches("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$")) {
                                if (!textField5.getText().isEmpty() && textField5.getText().matches("[0-9]*")) {
//                                    PartilhaClient client = new PartilhaClient(textField4.getText(), uiController);
//                                    client.start();
//                                    UDPClient udpclient = new UDPClient();
//                                    udpclient.start();
                                    JOptionPane.showMessageDialog(null, "Partilha Acedida");
                                } else {
                                    JOptionPane.showMessageDialog(null, "Verifique o valor da Porta");
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Verifique o endereço de IP");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Células não inseridas correctamente\n Ex: A1:B2");
                        }
                    }
                });
        add(button3);
    }
}