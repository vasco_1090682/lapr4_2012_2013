/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core;

import java.text.NumberFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fábio
 */
public class UpdateCellTest {

    public UpdateCellTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of updateCellContent method, of class UpdateCell.
     */
    @Test
    public void testUpdateCellContent() throws Exception {
        System.out.println("updateCellContent");
        Workbook workbook = new Workbook(1);
        Spreadsheet sheet = workbook.getSpreadsheet(0);
        Cell cell = sheet.getCell(new Address(0, 0));

        Value value = new Value(5);
        UpdateCell instance = new UpdateCell();
        instance.updateCellContent(cell, value);

        Value expResult = value; 
        Value result = cell.getValue();
        assertEquals(expResult.toString(NumberFormat.getInstance()), result.toString(NumberFormat.getInstance()));
    }
}