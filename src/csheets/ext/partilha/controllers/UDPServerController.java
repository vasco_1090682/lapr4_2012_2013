package csheets.ext.partilha.controllers;

import csheets.ext.partilha.Partilha;
import csheets.ext.partilha.UDPServer;


/**
 * Controlador do servidor UDP
 * @author Vasco Ribeiro
 */
public class UDPServerController {
    
    private UDPServer udpserver;
    
    /**
     * Método que cria o servidor UDP
     * @param p Partilha associada ao servidor
     */
    public void create(Partilha p){
        udpserver = new UDPServer(p);
    }
    
    /**
     * Método que dá início à thread
     */
    public void start(){
        udpserver.start();
    }
    
    /**
     * Método que termina a thread
     */
    public void stopServer(){
        udpserver.stopServer();
    }
    
    /**
     * Método de consulta
     * @return Partilha associada ao servidor
     */
    public Partilha getPartilhaServer(){
        return udpserver.getP();
    }    
}
