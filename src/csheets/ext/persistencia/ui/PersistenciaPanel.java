/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.persistencia.ui;

import csheets.core.Workbook;
import csheets.ext.partilha.ExtensionPartilha;
import csheets.ext.persistencia.XMLMap;
import csheets.ext.persistencia.hibernate.HibernateUtil;
import csheets.io.XMLCodec;
import csheets.ui.ctrl.UIController;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Tiago
 */
public class PersistenciaPanel extends JPanel
{
    private UIController uiController;
    private JPanel panel1 = new JPanel();
    private JLabel label1 = new JLabel(), label2 = new JLabel();
    private JComboBox comboBox1 = new JComboBox();
    private JButton button1 = new JButton(), button2 = new JButton(), button3 = new JButton(), button4 = new JButton();
    
    public PersistenciaPanel(UIController ui)
    {
        super(new GridLayout(8, 0));
        setName(ExtensionPartilha.NAME);
        uiController = ui;
        
        label1.setText("Versoes");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
        add(label1);
        
        panel1.setLayout(new GridLayout(1, 2));

        label2.setText("Versao escolhida: ");
        panel1.add(label2);
        
        comboBox1.setEnabled(false);
        panel1.add(comboBox1);
        add(panel1);
        button1.setText("Actualizar");
        button1.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        comboBox1.removeAllItems();
                        Session session = HibernateUtil.getSession().openSession();
                        org.hibernate.Query query = session.createQuery("from csheets.ext.persistencia.XMLMap");
                        List list = query.list();
                        if (list.size()!=0) 
                        {
                            for (Iterator iterator = list.iterator(); iterator.hasNext();)
                            {
                                 XMLMap xml = (XMLMap) iterator.next();
                                 comboBox1.setEnabled(true);
                                 comboBox1.addItem(xml.getTime());

                            }
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Nao a versoes do ficheiro");
                        }
                        session.close();
                    }
                });
        add(button1);
        
        button2.setText("Selecionar Versao");
        button2.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        Session session = HibernateUtil.getSession().openSession();
                        org.hibernate.Query query = session.createQuery("from csheets.ext.persistencia.XMLMap");
                        List list = query.list();
                        int lenght;
                        Object object = null;
                        if (list.size()!=0) 
                        {
                            for (Iterator iterator = list.iterator(); iterator.hasNext();)
                            {
                                 XMLMap xml = (XMLMap) iterator.next();
                                 if (comboBox1.getItemAt(comboBox1.getSelectedIndex()).equals(xml.getTime())) 
                                 {
                                        try 
                                        {
                                            lenght = (int)xml.getBlob().length();
                                            object = new java.io.ObjectInputStream(new java.io.ByteArrayInputStream(xml.getBlob().getBytes(1,lenght))).readObject();

                                        } 
                                        catch (SQLException ex)
                                        {
                                            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        catch(IOException ex)
                                        {
                                            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        catch (ClassNotFoundException ex) 
                                        {
                                             Logger.getLogger(PersistenciaPanel.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        Workbook workbook = (Workbook) object;
                                        uiController.setActiveWorkbook(workbook);
                                 }

                            }
                        }
                        session.close();
                    }
                });
        add(button2);
        
        button3.setText("Remover Versao");
        button3.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        Session session = HibernateUtil.getSession().openSession();
                        String hql = "delete from csheets.ext.persistencia.XMLMap where time = :time";
                        Query query = session.createQuery(hql);
                        String aux = comboBox1.getItemAt(comboBox1.getSelectedIndex()).toString();
                        query.setString("time",aux);
                        session.close();
                    }
                });
        add(button3);
        
        button4.setText("Tornar versão actual");
        button4.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        XMLMap map = null;
                        try 
                        {
                            try
                            {
                                map = new XMLMap(uiController.getActiveWorkbook());
                            } 
                            catch (IOException ex) 
                            {
                                Logger.getLogger(PersistenciaPanel.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } catch (SQLException ex) 
                        {
                            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Session session = HibernateUtil.getSession().openSession();
                        org.hibernate.Transaction tx = session.beginTransaction();
                        session.save(map);
                        tx.commit();
                        session.close();
                        uiController.setWorkbookModified(uiController.getActiveWorkbook());
                    }
                });
        add(button4);
    }
}
