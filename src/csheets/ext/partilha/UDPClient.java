package csheets.ext.partilha;

import java.io.IOException;
import java.net.*;
import java.util.*;
import javax.swing.*;

/**
 * Esta classe é responsável por criar um cliente de protocolo UDP que fica à escuta na rede
 * por servidores que estejam a anunciar a sua presença
 * @author Vasco Ribeiro
 */
public class UDPClient extends Thread {

    private static ArrayList<Partilha> servers = new ArrayList<Partilha>();
    private DefaultComboBoxModel<String> model;
    private boolean connected;

    /**
     * Construtor do cliente UDP
     * @param model Dados da ComboBox de partilhas existentes na rede local
     */
    public UDPClient(DefaultComboBoxModel<String> model) {
        this.model = model;
    }

    public void run() {
        try {
            connected = true;

            DatagramSocket clientSocket = new DatagramSocket(500);

            while (connected) {
                byte[] receiveData = new byte[200];
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket);
                String str = new String(receivePacket.getData());
                Partilha p = new Partilha(str.split(";")[0].trim(), str.split(";")[1].trim(), Partilha.PermissionsTypes.valueOf(str.split(";")[2].trim()), receivePacket.getAddress(), str.split(";")[3].trim().equals("online"));
                if (p.isOnline()) {
                    if (!containsServer(p)) {
                        servers.add(p);
                        model.addElement(p.getName() + " - " + p.getArea() + " - " + p.getPermission());
                    }
                } else {
                    p.setOnline(true); //p está offline, mas e preciso colocar aqui a online para poder remover da lista de servidores
                    if (containsServer(p)) {
                        removeServer(p);
                        model.removeElement(p.getName() + " - " + p.getArea() + " - " + p.getPermission());
                    }
                }
            }
            clientSocket.disconnect();
        } catch (SocketException ex) {
            JOptionPane.showMessageDialog(null, ex);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    /**
     * Método que verifica se uma partilha existe na lista de servidores a anunciar na rede
     * @param partilha Partilha respectiva do servidor que anuncia na rede
     * @return True ou false
     */
    private boolean containsServer(Partilha partilha) {
        for (Partilha p : servers) {
            if (p.getName().equals(partilha.getName()) && p.getArea().equals(partilha.getArea())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Dado um nome e área retorna a partilha correspondente
     * @param name Nome da partilha
     * @param area Area da partilha
     * @return Partilha que corresponde a uma nome e uma área
     */
    public Partilha getServer(String name, String area) {
        Partilha partilha = null;
        for (Partilha p : servers) {
            if (p.getName().equals(name) && p.getArea().equals(area)) {
                partilha = p;
            }
        }
        return partilha;
    }

    /**
     * Método que muda a variável connected para false fazendo com que o cliente termine a ligação
     */
    public void leaveServer() {
        connected = false;
    }
    
    /**
     * Remove uma dada partilha da lista de servidores
     * @param p Partilha a remover da lista de servidores 
     */
    private synchronized void removeServer(Partilha p) {
        for (Iterator<Partilha> it = servers.iterator(); it.hasNext();) {
            Partilha pa = it.next();
            if (pa.getName().equals(p.getName()) && pa.getArea().equals(p.getArea()) && pa.getServerIp().equals(p.getServerIp()) && pa.isOnline() == true) {
                it.remove();
            }
        }
    }
}
