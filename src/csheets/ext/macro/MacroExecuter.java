package csheets.ext.macro;

import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.compiler.FormulaCompiler;
import csheets.core.formula.lang.CellReference;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Fábio Queirós
 */
public class MacroExecuter extends Thread {

    private ControllerMacro ctrlMacro;
    private Macro macro;

    /**
     * Constructor of MacroExecuter
     *
     * @param ctrlMacro
     * @param macro
     */
    public MacroExecuter(ControllerMacro ctrlMacro, Macro macro) {
        this.ctrlMacro = ctrlMacro;
        this.macro = macro;
    }

    /**
     * start the run of macro
     */
    public void run() {
        try {
            this.runMacro(macro);
        } catch (MacroException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        } catch (FormulaCompilationException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        } catch (IllegalValueTypeException ex) {
            Logger.getLogger(MacroExecuter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Execute an expression compilating the values
     * @param cellAddress
     * @param formula
     * @param param
     * @throws ParseException
     * @throws FormulaCompilationException
     * @throws IllegalValueTypeException
     */
    public void executeExpression(String cellAddress, String formula, String[] param) throws ParseException, FormulaCompilationException, IllegalValueTypeException {
        try {
            if (cellAddress.startsWith("$")) {
                if (formula.matches("[$][0-9]*")) {
                    for (int i = 0; i < param.length; i++) {
                        if (param[i].startsWith(formula)) {
                            formula = param[i].split(",")[1];
                        }
                    }
                }
                String cellFake = "A1";
                CellReference cr = new CellReference(ctrlMacro.getUiCtrl().getActiveSpreadsheet(), cellFake);
                Cell c = cr.getCell();
                FormulaCompiler fc = FormulaCompiler.getInstance();
                String source;
                source = "#" + cellAddress + ":=" + formula;
                fc.compile(c, source);

            } else if (cellAddress.startsWith("exec")) {
                cellAddress = cellAddress.replace("(", "");
                cellAddress = cellAddress.replace(")", "");
                String[] ma = cellAddress.split(",");
                String pa = "";
                for (int i = 1; i < ma.length; i++) {
                    pa = pa + "$" + i + "=" + ma[i] + ",";
                }

                Macro m = ctrlMacro.getMacroGivenAName(ma[0].replace("exec", ""));
                m.setParam(pa);
                ctrlMacro.execMacro(m);
            } else {
                if (formula.matches("[$][0-9]*")) {
                    for (int i = 0; i < param.length; i++) {
                        if (param[i].startsWith(formula)) {
                            formula = param[i].split("=")[1];
                        }
                    }
                }
                CellReference cr = new CellReference(ctrlMacro.getUiCtrl().getActiveSpreadsheet(), cellAddress);
                FormulaCompiler fc = FormulaCompiler.getInstance();
                Cell c = cr.getCell();
                String source;
                source = "#" + cellAddress + ":=" + formula;
                //"#A5:=sum(A1:A2)"
                fc.compile(c, source);
            }

        } catch (ParseException ex) {
            throw new ParseException("Cell '" + cellAddress + "' doens't exist", 0);
        } catch (FormulaCompilationException ex) {
            throw new FormulaCompilationException("Rewrite the formula '" + formula + "'");
        }

    }

    /**
     * Run the macro received by parameter and execute all the expressions of the macro
     * @param macro
     * @throws MacroException
     * @throws ParseException
     * @throws FormulaCompilationException
     * @throws IllegalValueTypeException
     */
    public void runMacro(Macro macro) throws MacroException, ParseException, FormulaCompilationException, IllegalValueTypeException {
        String[] param = macro.getParam().split(",");
        for (String s : macro.getExpressions()) {
            if (s.startsWith("exec")) { // quando é run da 
                s = s + ":=0";
            }
            String[] str = s.split(":=");
            if (str.length == 2) {
                executeExpression(str[0], str[1], param);
            } else {
                throw new MacroException("The expression '" + s + "' is missing the ':=' operator");
            }
        }
    }
}
