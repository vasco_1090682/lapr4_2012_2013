package csheets.ext.partilha;

import java.net.InetAddress;

/**
 *
 * @author Vasco Ribeiro
 */
public class Partilha {

    /**
     * Variável do tipo enumerado que contém todas as permissões disponíveis para as partilhas
     */
    public enum PermissionsTypes {

        /**
         * O cliente apenas receberá informação, qualquer alteração à área de partilha feita pelo
         * cliente não será feita na partilha a que esteja ligado
         */
        Read,
        /**
         * Qualquer alteração feita pelo cliente à área de partilha, acontecerá também
         * na partilha a que está ligado
         */
        Write;
    }
    private String name;
    private String area;
    private String password;
    private PermissionsTypes permission;
    private InetAddress serverIp;
    private boolean online;

    //partilhas recebidas por um cliente
    /**
     * Este construtor é usado para contruir objectos Partilha do lado do cliente, ou seja,
     * partilhas que estão a ser anunciadas na rede e o cliente armazane-as para uso posterior
     * @param name Nome da partilha
     * @param area Area da partilha
     * @param permission Permissão da partilha
     * @param serverIp Servidor da origem da partilha
     * @param online Indica se a partilha está online ou não
     */
    public Partilha(String name, String area, Partilha.PermissionsTypes permission, InetAddress serverIp, boolean online) {
        this.name = name;
        this.area = area;
        this.permission = permission;
        this.serverIp = serverIp;
        this.online = online;
    }

    //partilhas feitas por este servidor
    /**
     * Este contrutor é usado do lado do servidor para armazenar objectos Partilha criados
     * pelo próprio servidor
     * @param name Nome da partilha
     * @param area Area da partilha
     * @param password Password da partilha
     * @param permission Permissão da partilha
     * @param online Indica se a partilha está online ou não
     * @throws PartilhaException
     */
    public Partilha(String name, String area, String password, PermissionsTypes permission, boolean online) throws PartilhaException {
        setName(name);
        setPassword(password);
        setArea(area);
        setPermission(permission);
        this.online = online;
    }

    /**
     *
     * @return Nome da partilha
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return Area da partilha
     */
    public String getArea() {
        return area;
    }

    /**
     *
     * @return Password da partilha
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @return Permissão da partilha
     */
    public PermissionsTypes getPermission() {
        return permission;
    }

    /**
     *
     * @return Ip da origem da partilha
     */
    public InetAddress getServerIp() {
        return serverIp;
    }

    /**
     * 
     * @return Se verdadeiro a partilha está online, caso contrário está offline
     */
    public boolean isOnline() {
        return online;
    }

    /**
     * Método modificador que valida o nome introduzido
     * @param name
     * @throws PartilhaException
     */
    public void setName(String name) throws PartilhaException {
        if (name == null || name.isEmpty()) {
            throw new PartilhaException("Enter a name");
        } else {
            this.name = name;
        }
    }

    /**
     * Método modificador que valida a área introduzida
     * @param area
     * @throws PartilhaException
     */
    public void setArea(String area) throws PartilhaException {
        if (area.split(":")[0].matches("[A-Z][0-9]+") && area.split(":")[1].matches("[A-Z][0-9]+")) {
            this.area = area;
        } else {
            throw new PartilhaException("Rewrite the area\n Ex: A1:B2");
        }

    }

    /**
     * Método modificador que valida a password introduzida
     * @param password
     * @throws PartilhaException
     */
    public void setPassword(String password) throws PartilhaException {
        if (password == null || password.isEmpty()) {
            throw new PartilhaException("Enter a password");
        } else {
            this.password = password;
        }

    }

    /**
     * Método modificador que valida a permissão introduzida
     * @param permission
     * @throws PartilhaException
     */
    public void setPermission(PermissionsTypes permission) throws PartilhaException {
        if (permission == Partilha.PermissionsTypes.Read || permission == Partilha.PermissionsTypes.Write){
            this.permission = permission;
        } else {
            throw new PartilhaException("Enter type of permission");
        }
        
    }

    /**
     * Método modificador
     * @param serverIp
     */
    public void setServerIp(InetAddress serverIp) {
        this.serverIp = serverIp;
    }

    /**
     * Método modificador
     * @param online
     */
    public void setOnline(boolean online) {
        this.online = online;
    }
}
