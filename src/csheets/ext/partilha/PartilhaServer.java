package csheets.ext.partilha;

import csheets.core.Cell;
import csheets.core.CellListener;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.lang.CellReference;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ui.ctrl.UIController;
import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * Esta classe é responsável por criar um servidor que usa o protocolo TCP, que
 * fica responvável por comunicar com um cliente TCP que posteriormente se irá
 * ligar a este servidor
 * @author Tiago Pires
 * @author Vasco Ribeiro
 */
public class PartilhaServer extends Thread {

    private DataOutputStream outstream;
    private DataInputStream instream;
    private ServerSocket serverSock;
    private Socket clntSock;
    private UIController uiController;
    private ArrayList<Cell> sharedcells = new ArrayList<Cell>();
    private int porta = 500;
    private int r1 = 0, c1 = 0, r2 = 0, c2 = 0;
    private boolean online;
    private Partilha p;

    /**
     * Contrutor do servidor TCP
     * @param p Partilha a que esta thread fica associada
     * @param ui UIController
     */
    public PartilhaServer(Partilha p, UIController ui) {
        this.p = p;
        uiController = ui;
    }

    @Override
    public void run() {
        try {
            online = true;
            setBondaries();
            celulasPartilhadas();
            addCellListener();
            addCellColor();

            serverSock = new ServerSocket(porta);
            clntSock = serverSock.accept();
            instream = new DataInputStream(clntSock.getInputStream());
            outstream = new DataOutputStream(clntSock.getOutputStream());

            String password = Encript.decode(instream.readUTF());
            if (password.equals(getP().getPassword())) {
                outstream.writeUTF("correct password");
                
                outstream.writeUTF(getP().getArea());
                outstream.writeInt(sharedcells.size());
                for (int i = 0; i < sharedcells.size(); i++) {
                    outstream.writeUTF(sharedcells.get(i).getContent());
                }

                while (online) {
                    int row = instream.readInt();
                    int column = instream.readInt();
                    String ctent = instream.readUTF();
                    try {
                        uiController.getActiveSpreadsheet().getCell(column, row).setContent(ctent);
                    } catch (FormulaCompilationException ex) {
                        JOptionPane.showMessageDialog(null, ex.toString());
                    }
                }
            } else {
                outstream.writeUTF("wrong password");
            }
            clntSock.close();
            serverSock.close();
            this.interrupt();

        } catch (SocketException ex) {
//            JOptionPane.showMessageDialog(null, ex.toString());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Server error", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que muda a variável online para false, fazendo com que o servidor se desligue
     */
    public void stopServer() {
        online = false;
    }

    /**
     * Método responsável por colocar num contentor as células que serão partilhas
     */
    private void celulasPartilhadas() {
        Cell cellescolhida;
        for (int i = c1; i <= c2; i++) {
            for (int j = r1; j <= r2; j++) {
                cellescolhida = uiController.getActiveSpreadsheet().getCell(i, j);
                sharedcells.add(cellescolhida);
            }
        }
    }

    /**
     * Adiciona um CellListener (propriedade que desencadeia ações quando há mudanças num dada célula) a cada uma das células
     */
    private void addCellListener() {
        for (int i = c1; i <= c2; i++) {
            for (int j = r1; j <= r2; j++) {
                uiController.getActiveSpreadsheet().getCell(i, j).addCellListener(getCellListener());
            }
        }
    }

    /**
     * Método que às células da área da partilha modifica a cor de fundo para melhor distinção 
     */
    private void addCellColor() {
        for (int i = c1; i <= c2; i++) {
            for (int j = r1; j <= r2; j++) {
                Cell c = uiController.getActiveSpreadsheet().getCell(i, j);
                StylableCell stylableCell = (StylableCell) c.getExtension(StyleExtension.NAME);
                stylableCell.setBackgroundColor(new Color(251, 248, 132));
            }
        }
        //O reset de uma célula da columa seguinte elimina a má formatação da cor nas células desejadas
        Cell cc1 = uiController.getActiveSpreadsheet().getCell((c2 + 1), r1);
        StylableCell stylableCell1 = (StylableCell) cc1.getExtension(StyleExtension.NAME);
        stylableCell1.resetStyle();

    }

    /**
     * Método define os limites da área de partilha
     */
    private void setBondaries() {
        try {
            CellReference aux = null;
            aux = new CellReference(uiController.getActiveSpreadsheet(), getP().getArea().split(":")[0]);
            r1 = aux.getCell().getAddress().getRow();
            c1 = aux.getCell().getAddress().getColumn();
            aux = new CellReference(uiController.getActiveSpreadsheet(), getP().getArea().split(":")[1]);
            r2 = aux.getCell().getAddress().getRow();
            c2 = aux.getCell().getAddress().getColumn();
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, "Error setting bondaries", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que envia para a partilha a que está ligado a informação de uma célula caso esta
     * tenha sido modificada nesta instância
     * @param cell Célula que sofreu a alteração
     */
    private void updateCeculasPartilhadas(Cell cell) {
        try {
            outstream.writeInt(cell.getAddress().getRow());
            outstream.writeInt(cell.getAddress().getColumn());
            outstream.writeUTF(cell.getContent());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error updating", "Error", JOptionPane.ERROR_MESSAGE);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Client closed connection", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que adiciona um trigger a uma célula, para fazer o desejado (neste caso mandar o update
     * para a instância a que está ligado) quando existe alguma alteração à célula
     * @return Trigger da célula
     */
    private CellListener getCellListener() {
        return new CellListener() {
            @Override
            public void valueChanged(Cell cell) {
            }

            @Override
            public void contentChanged(Cell cell) {
                updateCeculasPartilhadas(cell);
            }

            @Override
            public void dependentsChanged(Cell cell) {
            }

            @Override
            public void cellCleared(Cell cell) {
            }

            @Override
            public void cellCopied(Cell cell, Cell source) {
            }
        };
    }

    /**
     * Método de consulta
     * @return Partilha associada a este servidor
     */
    public Partilha getP() {
        return p;
    }
}
