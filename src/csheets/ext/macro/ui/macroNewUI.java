package csheets.ext.macro.ui;

import csheets.ext.macro.ControllerMacro;
import csheets.ext.macro.Macro;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;

public class macroNewUI extends JDialog {

    private JTextField textName;
    private JTextArea textExpressions;
    private JButton btSave, btCancel;
    private ControllerMacro ctrlMacro;
    DefaultComboBoxModel<String> model;

    public macroNewUI(JFrame fatherFrame, String title, Macro macro, ControllerMacro ctrlMacro, DefaultComboBoxModel<String> model) {
        super(fatherFrame, title, true);
        this.ctrlMacro = ctrlMacro;

        this.model = model;

        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));

        JPanel p1 = new JPanel();
        JLabel lbName = new JLabel("Name :");
        p1.add(lbName);

        textName = new JTextField();
        textName.setPreferredSize(new Dimension(200, 25));
        p1.add(textName);

        textExpressions = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textExpressions);
        scrollPane.setBounds(3, 3, 300, 200);

        JPanel p2 = new JPanel();
        btSave = new JButton("Save");
        btCancel = new JButton("Cancel");
        p2.add(btSave);
        p2.add(btCancel);

        p.add(p1, BorderLayout.NORTH);
        p.add(scrollPane, BorderLayout.CENTER);
        p.add(p2, BorderLayout.SOUTH);

        add(p);

        fillTextBoxes(macro);

        ProcessEvent pe = new ProcessEvent();
        btSave.addActionListener(pe);
        btCancel.addActionListener(pe);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(400, 300));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private class ProcessEvent implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btSave) {
                String name = textName.getText();
                if (ctrlMacro.verifyNameMacro(name)) {
                    String[] exps = textExpressions.getText().split("\n");

                    ArrayList<String> expressions = new ArrayList<String>();

                    for (int i = 0; i < exps.length; i++) {
                        expressions.add(exps[i]);
                    }

                    Macro macro = new Macro(name, "", expressions);
                    ctrlMacro.addMacro(macro);

                    for (int i = model.getSize(); i < ctrlMacro.getMacros().size(); i++) {
                        model.addElement(ctrlMacro.getMacros().get(i).getName());
                    }

                    dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "The name of this macro is repeted. Please choose other name");
                }
            } else {
                dispose();
            }
        }
    }

    public void fillTextBoxes(Macro macro) {
        if (macro != null) {
            textName.setText(macro.getName());

            String str = "";
            for (String s : macro.getExpressions()) {
                str = str + s + "\n";
            }
            textExpressions.setText(str);
        }
    }
}
