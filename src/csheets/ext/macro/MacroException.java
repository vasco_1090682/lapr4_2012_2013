package csheets.ext.macro;

public class MacroException extends Exception {

    public MacroException(String msg) {
        super(msg);
    }
}
