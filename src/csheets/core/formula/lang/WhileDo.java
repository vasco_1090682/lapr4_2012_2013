/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import static csheets.core.formula.lang.Do.parameters;
import java.lang.reflect.Method;

/**
 * Operador ciclo WhileDo
 * @author Cláudio
 */
public class WhileDo implements Function {
    

    private String identifier;
    private Method method;
   
 
    /**
     * Parametros usados pela função WhileDo
     */
    public static final FunctionParameter[] parameters = new FunctionParameter[] {
            new FunctionParameter(Value.Type.BOOLEAN, "Condição", false,
                    "A condition to evaluate before proceeding"),
		new FunctionParameter(Value.Type.UNDEFINED, "Expressão 1", false,
			"An expression to evaluate"),
               new FunctionParameter(Value.Type.UNDEFINED, "Expressão 2", false,
                         "An expression to evaluate")        
    };
    @Override
    public String getIdentifier() {
        return "WD";
    }
    
    /**
     *
     * @param identifier define identificador de WHILEDO
     */
    public void setIdentifier(String identifier){
        this.identifier = identifier;
    }
    @Override
    public Value applyTo(Expression[] arguments) throws IllegalValueTypeException {
        //@param v Valor que representa o valor da expressão final calculada
        Value v = new Value();
        v=arguments[2].evaluate();  
        return v;  
    }
    /**
     *
     */
    public FunctionParameter[] getParameters() {
		return parameters;
    }

    public boolean isVarArg() {
		return method.isVarArgs();
    }
    
}
