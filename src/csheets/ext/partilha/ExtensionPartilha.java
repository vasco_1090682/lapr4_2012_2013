/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.partilha;

import csheets.ext.Extension;
import csheets.ext.partilha.ui.UIExtensionPartilha;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Tiago
 */
public class ExtensionPartilha extends Extension
{
        /** The name of the extension */
	public static final String NAME = "Partilha de Folha";

	/**
	 * Creates a Partilha extension.
	 */
	public ExtensionPartilha() 
        {
		super(NAME);
	}
	
	/**
	 * Returns the user interface extension of this extension (an instance of the class {@link  csheets.ext.partilha.ui.UIExtensionPartilha}). <br/>
	 * In this extension example we are only extending the user interface.
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionPartilha(this, uiController);
	}
}
