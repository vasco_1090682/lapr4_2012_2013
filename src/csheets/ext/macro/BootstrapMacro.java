package csheets.ext.macro;

import java.util.ArrayList;

/**
 *
 * @author Fábio
 */
public class BootstrapMacro {
    /**
     *
     * @param ctrlMacro
     */
    public void createMacros(ControllerMacro ctrlMacro){
        ArrayList<String> exps1 = new ArrayList<String>();
        exps1.add("A1:=$1");
        exps1.add("A2:=$2");

        ArrayList<String> exps2 = new ArrayList<String>();
        exps2.add("exec(m1,200,500)");
        
        ArrayList<String> exps3 = new ArrayList<String>();
        exps3.add("A5:=sum(A1:A2)");
        
        ArrayList<String> exps4 = new ArrayList<String>();
        exps4.add("A5=sum(A1:A2)");
        
        ArrayList<String> exps5 = new ArrayList<String>();
        exps5.add("A:=sum(A1:A2)");
        
        ArrayList<String> exps6 = new ArrayList<String>();
        exps6.add("A5:=sul(A1:A2)");
        
        Macro m1 = new Macro("m1","", exps1);
        Macro m2 = new Macro("m2","", exps2);
        Macro m3 = new Macro("m3","", exps3);
        Macro m4 = new Macro("m4","", exps4);
        Macro m5 = new Macro("m5","", exps5);
        Macro m6 = new Macro("m6","", exps6);
        
        ctrlMacro.addMacro(m1);
        ctrlMacro.addMacro(m2);
        ctrlMacro.addMacro(m3);
        ctrlMacro.addMacro(m4);
        ctrlMacro.addMacro(m5);
        ctrlMacro.addMacro(m6);
    }
}
