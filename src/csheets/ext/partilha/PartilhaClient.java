package csheets.ext.partilha;

import csheets.core.Cell;
import csheets.core.CellListener;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.lang.CellReference;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import csheets.ui.ctrl.UIController;
import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * Esta classe é responsável por criar uma cliente que usa o protocolo TCP, que
 * fica responsável de comunicar com o servidor TCP para transmissão de dados
 * entre as duas partilhas
 *
 * @author Tiago Pires
 * @author Vasco Ribeiro
 */
public class PartilhaClient extends Thread {

    private DataOutputStream outstream;
    private DataInputStream instream;
    private Socket sock;
    private UIController uiController;
    private String arearecebida;
    private ArrayList<String> content = new ArrayList<String>();
    private int porta = 500;
    private int r1 = 0, c1 = 0, r2 = 0, c2 = 0;
    private boolean connected;
    private Partilha p;
    private String password;
    private DefaultListModel<Object> modelServersConnectedToList;

    /**
     * Construtor do cliente TCP
     * @param p Partilha a que esta thread fica associada
     * @param password Password de acesso ao servidor TCP
     * @param modelServersConnectedToList Dados da JList de servidores a que a instância está conectada
     * @param ui UIController
     */
    public PartilhaClient(Partilha p, String password, DefaultListModel<Object> modelServersConnectedToList, UIController ui) {
        this.p = p;
        this.password = Encript.encode(password);
        this.modelServersConnectedToList = modelServersConnectedToList;
        uiController = ui;
    }

    @Override
    public void run() {
        try {
            connected = true;
            sock = new Socket(p.getServerIp(), porta);
            instream = new DataInputStream(sock.getInputStream());
            outstream = new DataOutputStream(sock.getOutputStream());

            outstream.writeUTF(password);

            String validation = instream.readUTF();

            if (validation.equals("correct password")) {
                modelServersConnectedToList.addElement(p.getName() + " - " + p.getArea() + " - " + p.getPermission());

                arearecebida = instream.readUTF();
                setBondaries();
                if (p.getPermission() == Partilha.PermissionsTypes.Write) {
                    addCellListener();
                }
                addCellColor();

                int tamanho = instream.readInt();

                String leitura;
                for (int i = 0; i < tamanho; i++) {
                    leitura = instream.readUTF();
                    content.add(leitura);
                }
                setCelulas(c1, r1, c2, r2, content);

                while (connected) {
                    int row = instream.readInt();
                    int column = instream.readInt();
                    String ctent = instream.readUTF();
                    try {
                        uiController.getActiveSpreadsheet().getCell(column, row).setContent(ctent);
                    } catch (FormulaCompilationException ex) {
                        JOptionPane.showMessageDialog(null, ex.toString());
                    }
                }
                instream.close();
                outstream.close();
                sock.close();
                this.interrupt();
            }
            this.interrupt();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Server closed");
        }
    }

    /**
     * Método que adiciona às células que pertencem à área da partilha o conteúdo
     * da partilha de origem
     */
    private void setCelulas(int col1, int row1, int col2, int row2, ArrayList<String> conteudo) {
        int cont = 0;
        for (int i = col1; i <= col2; i++) {
            for (int j = row1; j <= row2; j++) {
                try {
                    uiController.getActiveSpreadsheet().getCell(i, j).setContent(conteudo.get(cont));
                } catch (FormulaCompilationException ex) {
                    JOptionPane.showMessageDialog(null, "Error receiving first cells", "Error", JOptionPane.ERROR_MESSAGE);
                }
                cont++;
            }
        }
    }

    /**
     * Método que muda a variável connected para false fazendo com que o cliente termine a ligação
     */
    public void leaveServer() {
        connected = false;
    }

    /**
     * Adiciona um CellListener (propriedade que desencadeia ações quando há mudanças num dada célula) a cada uma das células
     */
    private void addCellListener() {
        for (int i = c1; i <= c2; i++) {
            for (int j = r1; j <= r2; j++) {
                uiController.getActiveSpreadsheet().getCell(i, j).addCellListener(getCellListener());
            }
        }
    }
    
    /**
     * Método que às células da área da partilha modifica a cor de fundo para melhor distinção 
     */
    private void addCellColor() {
        for (int i = c1; i <= c2; i++) {
            for (int j = r1; j <= r2; j++) {
                Cell c = uiController.getActiveSpreadsheet().getCell(i, j);
                StylableCell stylableCell = (StylableCell) c.getExtension(StyleExtension.NAME);
                stylableCell.setBackgroundColor(new Color(251, 248, 132));
            }
        }
        //O reset de uma célula da columa seguinte elimina a má formatação da cor nas células desejadas
        Cell cc1 = uiController.getActiveSpreadsheet().getCell((c2 + 1), r1);
        StylableCell stylableCell1 = (StylableCell) cc1.getExtension(StyleExtension.NAME);
        stylableCell1.resetStyle();
    }

    /**
     * Método define os limites da área de partilha
     */
    private void setBondaries() {
        try {
            CellReference cellref = null;
            cellref = new CellReference(uiController.getActiveSpreadsheet(), arearecebida.split(":")[0]);
            r1 = cellref.getCell().getAddress().getRow();
            c1 = cellref.getCell().getAddress().getColumn();
            cellref = new CellReference(uiController.getActiveSpreadsheet(), arearecebida.split(":")[1]);
            r2 = cellref.getCell().getAddress().getRow();
            c2 = cellref.getCell().getAddress().getColumn();
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, "Error setting bondaries", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que envia para a partilha a que está ligado a informação de uma célula caso esta
     * tenha sido modificada nesta instância
     * @param cell Célula que sofreu a alteração
     */
    private void updateCeculasPartilhadas(Cell cell) {
        try {
            outstream.writeInt(cell.getAddress().getRow());
            outstream.writeInt(cell.getAddress().getColumn());
            outstream.writeUTF(cell.getContent());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error updating", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que adiciona um trigger a uma célula, para fazer o desejado (neste caso mandar o update
     * para a instância a que está ligado) quando existe alguma alteração à célula
     * @return Trigger da célula
     */
    private CellListener getCellListener() {
        return new CellListener() {
            @Override
            public void valueChanged(Cell cell) {
            }

            @Override
            public void contentChanged(Cell cell) {
                updateCeculasPartilhadas(cell);
            }

            @Override
            public void dependentsChanged(Cell cell) {
            }

            @Override
            public void cellCleared(Cell cell) {
            }

            @Override
            public void cellCopied(Cell cell, Cell source) {
            }
        };
    }
}
