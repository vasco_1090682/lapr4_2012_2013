package csheets.ext.partilha.controllers;

import csheets.ext.partilha.Partilha;
import csheets.ext.partilha.PartilhaServer;
import csheets.ui.ctrl.UIController;

/**
 * Controlador do servidor TCP
 * @author Vasco Ribeiro
 */
public class PartilhaServerController {
    
    private PartilhaServer tcpserver;
    
    /**
     * Método que cria o servidor TCP
     * @param p Partilha associada ao servidor
     * @param ui UIController
     */
    public void create(Partilha p, UIController ui){
        tcpserver = new PartilhaServer(p, ui);
    }
    
    /**
     * Método que dá início à thread
     */
    public void start(){
        tcpserver.start();
    }
    
    /**
     * Método que termina a thread
     */
    public void stopServer(){
        tcpserver.stopServer();
    }
    
    /**
     * Método de consulta
     * @return Partilha associada ao servidor
     */
    public Partilha getPartilhaServer(){
        return tcpserver.getP();
    }    
}
