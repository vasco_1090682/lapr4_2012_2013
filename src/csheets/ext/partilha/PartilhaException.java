package csheets.ext.partilha;

/**
 * Classe do tipo Exception que irá controlar algumas excepções específicas
 * da parte de partilhas da aplicação
 * @author Vasco Ribeiro
 */
public class PartilhaException extends Exception {

    /**
     * Excepção do tipo PartilhaException
     * @param msg Mensagem que aparece quando a excepção é detectada
     */
    public PartilhaException(String msg) {
        super(msg);
    }
}
