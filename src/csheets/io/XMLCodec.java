/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.io;

import csheets.core.Workbook;
import csheets.ext.persistencia.XMLMap;
import csheets.ext.persistencia.hibernate.HibernateUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlProducer;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;
import org.xml.sax.InputSource;

/**
 *
 * @author Cláudio @1090550
 * @author Tiago Pires
 */
public class XMLCodec implements Codec 
{

    /**
     * Cria um novo Codec XML
     */

    public void write(Workbook workbook, OutputStream stream) throws IOException 
    {
        XMLMap map = null;
        try 
        {
            map = new XMLMap(workbook);
        } catch (SQLException ex) 
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        Session session = HibernateUtil.getSession().openSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        session.save(map);
        tx.commit();
        session.close();
        Connection jdbcConnection = null;
        try 
        {
            jdbcConnection = DriverManager.getConnection("jdbc:hsqldb:mem:cleansheets", "sa", "");
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        IDatabaseConnection connection = null;
        try
        {
            connection = new DatabaseConnection(jdbcConnection);
        } catch (DatabaseUnitException ex) {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        IDataSet fullDataSet = null;
        try
        {
            fullDataSet = connection.createDataSet();
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        try 
        {
            FlatXmlDataSet.write(fullDataSet, stream);
        }
        catch (DataSetException ex) 
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }


    //Lê ficheiro xml
    public Workbook read(InputStream stream) throws IOException, ClassNotFoundException 
    {
        Workbook workbook = null;
        
        FlatXmlDataSet dataset = null;
        try 
        {
            dataset = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(stream)));
        } 
        catch (DataSetException ex)
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        Connection jdbcConnection = null;
        try 
        {
            jdbcConnection = DriverManager.getConnection("jdbc:hsqldb:mem:cleansheets", "sa", "");
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        IDatabaseConnection connection = null;
        try 
        {
            connection = new DatabaseConnection(jdbcConnection);
        }
        catch (DatabaseUnitException ex)
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        Session session = HibernateUtil.getSession().openSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        try
        {
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
        }
        catch (DatabaseUnitException ex) 
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<Workbook> wkbs = new ArrayList<Workbook>();
        org.hibernate.Query query = session.createQuery("from csheets.ext.persistencia.XMLMap");
        List list = query.list();
        Object object = null;
        int lenght;
        for (Iterator iterator = list.iterator(); iterator.hasNext();)
        {
            XMLMap xml = (XMLMap) iterator.next();
            try 
            {
                lenght = (int)xml.getBlob().length();
                object = new java.io.ObjectInputStream(new java.io.ByteArrayInputStream(xml.getBlob().getBytes(1,lenght))).readObject();
            } catch (SQLException ex)
            {
                Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
            }
            workbook = (Workbook) object;
            wkbs.add(workbook);
        }
        workbook = wkbs.get(wkbs.size()-1);
        try 
        {
            connection.close();
        }
        catch (SQLException ex) 
        {
            Logger.getLogger(XMLCodec.class.getName()).log(Level.SEVERE, null, ex);
        }
        tx.commit();
        session.close();
        return workbook;
    }
}
