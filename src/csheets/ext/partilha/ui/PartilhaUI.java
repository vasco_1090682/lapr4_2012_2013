package csheets.ext.partilha.ui;

import csheets.ext.partilha.*;
import csheets.ext.partilha.controllers.*;
import csheets.ui.ctrl.UIController;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.*;

/**
 * Classe responsável pela sidebar de partilhas
 * @author Tiago Pires
 * @author Vasco Ribeiro
 */
public class PartilhaUI extends JPanel {

    private JTextField textName, textArea;
    private JPasswordField passwordField;
    private JButton activateServer, deactivateServer, findServers, joinServer, leaveServer;
    private JComboBox<String> cmbServers;
    private JComboBox<Partilha.PermissionsTypes> cmbPermissions;
    private JList<Object> listMyServers, listServersConnectedTo;
    private UIController uiController;
    private DefaultComboBoxModel<String> modelServers;
    private DefaultListModel<Object> modelServersConnectedToList, modelMyServers;
    private PartilhaServerController tcpServerController;
    private PartilhaClientController tcpClientController;
    private UDPServerController udpServerController;
    private UDPClientController udpClientController;
    private ArrayList<PartilhaServerController> serversTCP;
    private ArrayList<UDPServerController> serversUDP;

    /**
     * Construtor da sidebar partilhas
     * @param uiController
     */
    public PartilhaUI(UIController uiController) {
        super();
        setName(ExtensionPartilha.NAME);
        this.uiController = uiController;
        serversTCP = new ArrayList<PartilhaServerController>();
        serversUDP = new ArrayList<UDPServerController>();

        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 80, 10, 80));

        JPanel p1 = new JPanel(new GridLayout(5, 1));
        p1.setBorder(BorderFactory.createTitledBorder("Server"));

        JPanel p1_1 = new JPanel();
        JLabel lbName = new JLabel("            Name:");
        textName = new JTextField();
        textName.setPreferredSize(new Dimension(175, 25));
        p1_1.add(lbName);
        p1_1.add(textName);

        JPanel p1_2 = new JPanel();
        JLabel lbPassword = new JLabel("    Password:");
        passwordField = new JPasswordField();
        passwordField.setPreferredSize(new Dimension(175, 25));
        p1_2.add(lbPassword);
        p1_2.add(passwordField);

        JPanel p1_3 = new JPanel();
        JLabel lbArea = new JLabel("              Area:");
        textArea = new JTextField();
        textArea.setPreferredSize(new Dimension(175, 25));
        p1_3.add(lbArea);
        p1_3.add(textArea);

        JPanel p1_4 = new JPanel();
        JLabel lbPermissions = new JLabel("Permissions:");
        cmbPermissions = new JComboBox<Partilha.PermissionsTypes>(Partilha.PermissionsTypes.values());
        cmbPermissions.setPreferredSize(new Dimension(175, 25));
        cmbPermissions.setSelectedIndex(-1);
        p1_4.add(lbPermissions);
        p1_4.add(cmbPermissions);

        JPanel p1_5 = new JPanel();
        activateServer = new JButton("Activate");
        p1_5.add(activateServer);

        p1.add(p1_1);
        p1.add(p1_2);
        p1.add(p1_3);
        p1.add(p1_4);
        p1.add(p1_5);


        JPanel p2 = new JPanel(new BorderLayout());
        p2.setBorder(BorderFactory.createTitledBorder("My servers"));

        JPanel p2_1 = new JPanel();
        modelMyServers = new DefaultListModel<Object>();
        listMyServers = new JList<Object>(modelMyServers);
        listMyServers.setFixedCellWidth(300);
        JScrollPane scrollPane1 = new JScrollPane(listMyServers);
        p2_1.add(scrollPane1);

        JPanel p2_2 = new JPanel();
        deactivateServer = new JButton("Deactivate");
        deactivateServer.setEnabled(false);
        p2_2.add(deactivateServer);

        p2.add(p2_1, BorderLayout.NORTH);
        p2.add(p2_2, BorderLayout.SOUTH);

        JPanel p3 = new JPanel(new BorderLayout());
        p3.setBorder(BorderFactory.createTitledBorder("Client"));

        JPanel p3North = new JPanel(new GridLayout(2, 1));

        JPanel p3_1 = new JPanel();
        JLabel chooseServer = new JLabel("Choose server:");
        modelServers = new DefaultComboBoxModel<String>();
        cmbServers = new JComboBox<String>(modelServers);
        cmbServers.setPreferredSize(new Dimension(175, 25));
        cmbServers.setEnabled(false);
        p3_1.add(chooseServer);
        p3_1.add(cmbServers);

        JPanel p3_2 = new JPanel();
        findServers = new JButton("Find servers");
        joinServer = new JButton("Join server");
        joinServer.setEnabled(false);
        p3_2.add(findServers);
        p3_2.add(joinServer);

        p3North.add(p3_1);
        p3North.add(p3_2);

        JPanel p3South = new JPanel(new BorderLayout());

        JPanel p3_3 = new JPanel();
        JLabel lbConnectedTo = new JLabel("Connected to:                                                                           ");
        p3_3.add(lbConnectedTo);

        JPanel p3_4 = new JPanel();
        modelServersConnectedToList = new DefaultListModel<Object>();
        listServersConnectedTo = new JList<Object>(modelServersConnectedToList);
        listServersConnectedTo.setFixedCellWidth(300);
        JScrollPane scrollPane2 = new JScrollPane(listServersConnectedTo);
        p3_4.add(scrollPane2);

        JPanel p3_5 = new JPanel();
        leaveServer = new JButton("Leave");
        leaveServer.setEnabled(false);
        p3_5.add(leaveServer);

        p3South.add(p3_3, BorderLayout.NORTH);
        p3South.add(p3_4, BorderLayout.CENTER);
        p3South.add(p3_5, BorderLayout.SOUTH);

        p3.add(p3North, BorderLayout.NORTH);
        p3.add(p3South, BorderLayout.SOUTH);

        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.CENTER);
        p.add(p3, BorderLayout.SOUTH);

        add(p);

        ProcessEvent pe = new ProcessEvent();
        activateServer.addActionListener(pe);
        deactivateServer.addActionListener(pe);
        findServers.addActionListener(pe);
        joinServer.addActionListener(pe);
        leaveServer.addActionListener(pe);
    }

    private class ProcessEvent implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == activateServer) {
                try {
                    tcpServerController = new PartilhaServerController();
                    udpServerController = new UDPServerController();

                    Partilha p1 = new Partilha(textName.getText(), textArea.getText(), new String(passwordField.getPassword()), (Partilha.PermissionsTypes) cmbPermissions.getSelectedItem(), true);

                    //cria o servidor da partilha propriamente dita
                    tcpServerController.create(p1, uiController);
                    serversTCP.add(tcpServerController);
                    tcpServerController.start();


                    //cria o servidor udp que vai anunciar na rede local a partilha
                    udpServerController.create(p1);
                    serversUDP.add(udpServerController);
                    udpServerController.start();


                    //adiciona à lista de partilhas que esta instância disponibiliza (é servidor) esta partilha
                    modelMyServers.addElement(p1.getName() + " - " + p1.getArea() + " - " + p1.getPermission());


                    textName.setText("");
                    textArea.setText("");
                    passwordField.setText("");
                    cmbPermissions.setSelectedIndex(-1);
                    deactivateServer.setEnabled(true);
                    cmbServers.setEnabled(false);
                    findServers.setEnabled(false);
                    joinServer.setEnabled(false);
                    leaveServer.setEnabled(false);
                } catch (PartilhaException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }

            } else if (e.getSource() == deactivateServer) {
                //remover o servidos da lista e fechar os servidores TCP e UDP
                int intServerIndex = listMyServers.getSelectedIndex();
                String strName = ((String) listMyServers.getSelectedValue()).split("-")[0].trim();
                removeServerTCP(strName);
                removeServerUDP(strName);
                modelMyServers.remove(intServerIndex);

                findServers.setEnabled(false);
            } else if (e.getSource() == findServers) {
                udpClientController = new UDPClientController();
                udpClientController.create(modelServers);
                udpClientController.start();

                textName.setEnabled(false);
                textArea.setEnabled(false);
                passwordField.setEnabled(false);
                cmbPermissions.setEnabled(false);
                cmbServers.setEnabled(true);
                activateServer.setEnabled(false);
                findServers.setEnabled(false);
                joinServer.setEnabled(true);
            } else if (e.getSource() == joinServer) {

                tcpClientController = new PartilhaClientController();
                String str = (String) cmbServers.getSelectedItem();
                String name = str.split("-")[0].trim();
                String area = str.split("-")[1].trim();
                Partilha p2 = udpClientController.getServer(name, area);

                //cria janela para receber password
                JPanel p_pwd = new JPanel();
                JPasswordField pwd = new JPasswordField();
                pwd.setPreferredSize(new Dimension(175, 25));
                p_pwd.add(pwd);
                JOptionPane.showMessageDialog(null, p_pwd, "Enter password", JOptionPane.PLAIN_MESSAGE);
                String password = new String(pwd.getPassword());
                tcpClientController.create(p2, password, modelServersConnectedToList, uiController);
                tcpClientController.start();

                textName.setEnabled(false);
                textArea.setEnabled(false);
                activateServer.setEnabled(false);
                cmbServers.setEnabled(false);
                findServers.setEnabled(false);
                joinServer.setEnabled(false);
                leaveServer.setEnabled(true);
            } else if (e.getSource() == leaveServer) {
                udpClientController.leaveServer();
                tcpClientController.leaveServer();
                int intServerIndex = listServersConnectedTo.getSelectedIndex();
                modelServersConnectedToList.remove(intServerIndex);
                
                activateServer.setEnabled(false);
                cmbServers.setEnabled(false);
                findServers.setEnabled(false);
                joinServer.setEnabled(false);
                leaveServer.setEnabled(false);
            }
        }
    }

    /**
     * Método que desliga o servidor TCP e o remove da lista de servidores TCP
     * @param name Nome da partilha associada ao servidor TCP
     */
    private void removeServerTCP(String name) {
        for (Iterator<PartilhaServerController> it = serversTCP.iterator(); it.hasNext();) {
            PartilhaServerController psc1 = it.next();
            if (psc1.getPartilhaServer().getName().equals(name)) {
                psc1.stopServer();
                it.remove();
            }
        }
    }

    /**
     * Método que desliga o servidor UDP e o remove da lista de servidores UDP
     * @param name Nome da partilha associada ao servidor UDP
     */
    private void removeServerUDP(String name) {
        for (Iterator<UDPServerController> it = serversUDP.iterator(); it.hasNext();) {
            UDPServerController usc1 = it.next();
            if (usc1.getPartilhaServer().getName().equals(name)) {
                usc1.stopServer();
                it.remove();
            }
        }
    }
}
