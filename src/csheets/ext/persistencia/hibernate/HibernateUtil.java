/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.persistencia.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;


/**
 *
 * @author Tiago
 */
public class HibernateUtil 
{
    private static SessionFactory session;
    
    public static SessionFactory getSession()
    {
        if (session == null) 
        {
            session = new AnnotationConfiguration().configure("csheets/ext/persistencia/hibernate/hibernate.cfg.xml").buildSessionFactory();
        }

        return session;
    }
}
